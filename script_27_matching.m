%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

function [EMS] = script_27_matching(Input,EMS)

	arguments
        Input struct
        EMS struct
	end

	threshold = 1e-5;

%% green energy	
	supply = EMS.Variables.power_local_supply_green;
	demand = EMS.Variables.power_local_demand_green;

	demand = demand - supply;
	supply = -demand;

	demand(demand<threshold) = 0;
	supply(supply<threshold) = 0;

	net_traded = zeros(size(demand,1),size(demand,2),size(demand,2));
	for time = 1:size(demand,1)
		[demand_power,demand_index] = sort(demand(time,:),'descend');
		[supply_power,supply_index] = sort(supply(time,:),'descend');

		s_idx = 1;
		d_idx = 1;
		while d_idx < length(demand_power) && s_idx < length(demand_power)
			net_supply = supply_power(s_idx) - demand_power(d_idx);
			net_traded(time,supply_index(s_idx),demand_index(d_idx)) = min(supply_power(s_idx),demand_power(d_idx));
			if net_supply > 0
				supply_power(s_idx) = supply_power(s_idx) - min(supply_power(s_idx),demand_power(d_idx));
				d_idx = d_idx+1;
			else
				demand_power(d_idx) = demand_power(d_idx) - min(supply_power(s_idx),demand_power(d_idx));
				s_idx = s_idx+1;
			end
		end
		clear demand_power demand_index supply_power supply_index d_idx s_idx net_supply;
	end
	clear time demand supply;
	power_trade_green = net_traded;
	clear net_traded;

%% green energy
	supply = EMS.Variables.power_local_supply_grey;
	demand = EMS.Variables.power_local_demand_grey;

	demand = demand - supply;
	supply = -demand;

	demand(demand<threshold) = 0;
	supply(supply<threshold) = 0;

	net_traded = zeros(size(demand,1),size(demand,2),size(demand,2));
	for time = 1:size(demand,1)
		[demand_power,demand_index] = sort(demand(time,:),'descend');
		[supply_power,supply_index] = sort(supply(time,:),'descend');

		s_idx = 1;
		d_idx = 1;
		while d_idx < length(demand_power) && s_idx < length(demand_power)
			net_supply = supply_power(s_idx) - demand_power(d_idx);
			net_traded(time,supply_index(s_idx),demand_index(d_idx)) = min(supply_power(s_idx),demand_power(d_idx));
			if net_supply > 0
				supply_power(s_idx) = supply_power(s_idx) - min(supply_power(s_idx),demand_power(d_idx));
				d_idx = d_idx+1;
			else
				demand_power(d_idx) = demand_power(d_idx) - min(supply_power(s_idx),demand_power(d_idx));
				s_idx = s_idx+1;
			end
		end
		clear demand_power demand_index supply_power supply_index d_idx s_idx net_supply;
	end
	clear time threshold demand supply;
	power_trade_grey = net_traded;
	clear net_traded;

%% update local and global power
	EMS.Variables.power_local_trade_green = power_trade_green;
	EMS.Variables.power_local_trade_grey = power_trade_grey;

	EMS.Variables.power_local_supply_green = squeeze(sum(power_trade_green,3));
	EMS.Variables.power_local_demand_green = squeeze(sum(power_trade_green,2));
	EMS.Variables.power_local_supply_grey = squeeze(sum(power_trade_grey,3));
	EMS.Variables.power_local_demand_grey = squeeze(sum(power_trade_grey,2));

	EMS.Variables.power_global_supply_green = EMS.Variables.power_supply_green - EMS.Variables.power_local_supply_green;
	EMS.Variables.power_global_supply_grey = EMS.Variables.power_supply_grey - EMS.Variables.power_local_supply_grey;
	EMS.Variables.power_global_demand_grey = EMS.Variables.power_demand_grey - EMS.Variables.power_local_demand_grey - EMS.Variables.power_local_demand_green;

end