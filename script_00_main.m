%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

	clearvars -except config*;
	close all; clc; tic;
	Input.system_time = datestr(now,'yyyymmddHHMMSS');
	load('setting.mat');
% 	rng('default')

%% initialization
% general parameters
	Input.sample_time					= 15 / 60; % [h] length of sample time
	Input.optimization_time				= 24; % [h] length of optimization period
	Input.trading_horizon				= Input.optimization_time / 2; % [h] length of trading horizon
	Input.rolling_horizon				= Input.optimization_time / 2; % [h] length of rolling horizon period
	Input.simulation_time				= 10 * 24;%config.simulation_time(config_i) * 24; % [h] length of simulation period
	Input.simulation_start				= 90 * 24; % [h] start time of simulation (effects profiles)
	Input.entities						= config.entities(config_i); % [1] number of participating entities (households)
	Input.trading_factor				= 0.5; % [0..1] values above 0.5 are favourable for net energy suppliers
	Input.trading_surcharge				= 0.00; % [1] percentage of relative trading surcharge
	Input.prediction_error				= 0; % [1] 1, to consider prediction errors; 0, otherwise
	Input.energy_offer_minimum			= 0.00; % [kWh] minimum offered energy per sample time
	Input.energy_offer_increment		= 0.00; % [kWh] increment of offered energy per sample time
	Input.energy_transaction_minimum	= 0.0; % [kWh] minimum traded energy per sample time
	Input.energy_transaction_increment	= 0.0; % [kWh] increment of traded energy per sample time

	Input.soh_caluclation				= true; % true, if soh calculations should be executed
	Input.save_results					= true; % true, if results should be saved
	Input.plot_png						= false * Input.save_results; % true, if plots should be exported as png files
	Input.plot_pdf						= false * Input.save_results; % true, if plots should be exported as pdf files
	Input.algorithm						= 'linprog'; % true, if available gurobi solver should be used
	Input.gurobi_solver					= true; % true, if available gurobi solver should be used
	Input.optimization_full				= false; % save full optimization data
	Input.path_results					= 'results\'; % path of results folder
	Input.max_optimization_time			= 30; % [s] maximum time per optimization
	Input.max_relative_optimization_gap	= 5e-3; % [1] maximum relative gap per optimization

	pv_system							= config.pv_system(config_i); % define share of pv systems
		Input.pv_system = [ones(1,round(pv_system*Input.entities,0)),zeros(1,round((1-pv_system)*Input.entities,0))];
% 		Input.pv_system = Input.pv_system(randperm(length(Input.pv_system)));

	pv_green							= 1; % define share of subsidized pv system
		Input.pv_green = zeros(1,Input.entities);
		[~,idx1] = find(Input.pv_system==1); idx1 = idx1(randperm(length(idx1)));
		for i = 1:round(min(pv_system,pv_green)*Input.entities,0)
			Input.pv_green(idx1(i)) = 1;
		end
		clear i idx*;

	stationary_storage					= config.stationary_storage(config_i); % define share of stationary energy storage system
% 		Input.stationary_storage = zeros(1,Input.entities);
% 		[~,idx1] = find(Input.pv_system==1); idx1 = idx1(randperm(length(idx1)));
% 		[~,idx0] = find(Input.pv_system==0); idx0 = idx0(randperm(length(idx0)));
% 		if stationary_storage <= pv_system
% 			for i = 1:round(stationary_storage*Input.entities,0)
% 				Input.stationary_storage(idx1(i)) = 1;
% 			end
% 		else
% 			Input.stationary_storage(idx1) = 1;
% 			for i = 1:round(stationary_storage*Input.entities,0)-round(pv_system*Input.entities,0)
% 				Input.stationary_storage(idx0(i)) = 1;
% 			end
% 		end
% 		clear i idx*;
		Input.stationary_storage = [ones(1,round(stationary_storage*Input.entities,0)),zeros(1,round((1-stationary_storage)*Input.entities,0))];

	electric_vehicle					= config.electric_vehicle(config_i); % define share of electric vehicles
		Input.electric_vehicle = [ones(1,round(electric_vehicle*Input.entities,0)),zeros(1,round((1-electric_vehicle)*Input.entities,0))];
		Input.electric_vehicle = Input.electric_vehicle(randperm(length(Input.electric_vehicle)));

	Input.ev_bidirectional				= zeros(size(electric_vehicle)); % define share of bidirectional electric vehicles
		clear i idx*;

% 	Input.profile_load = 137:1:211;
% 		Input.profile_load = Input.profile_load(randperm(length(Input.profile_load)));
% 		Input.profile_load = Input.profile_load(1:Input.entities);
		Input.profile_load = profile_load(1:Input.entities);

% 	Input.profile_pv = 5;
% 		Input.profile_pv = repmat(Input.profile_pv,1,ceil(Input.entities/length(Input.profile_pv)));
% 		Input.profile_pv = Input.profile_pv(randperm(length(Input.profile_pv)));
% 		Input.profile_pv = Input.profile_pv(1:Input.entities);
		Input.profile_pv = profile_pv(1:Input.entities);

% 	Input.profile_ev = 1:1:200;
% 		Input.profile_ev = Input.profile_ev(randperm(length(Input.profile_ev)));
% 		Input.profile_ev = Input.profile_ev(1:Input.entities);
		Input.profile_ev = profile_ev(1:Input.entities);

	clear pv_system pv_green stationary_storage electric_vehicle ev_bidirectional;

% scenario name
	Input.scenario_name	= ['FlexOpt',...
		'_t',sprintf('%04d',round(Input.simulation_time/24,0)),...
		'_p',sprintf('%03d',round(Input.entities,0)),...
		'_pv',sprintf('%03d',round(sum(Input.pv_system)/Input.entities*100,0)),...
		'_',sprintf('%03d',round(sum(Input.pv_green)/Input.entities*100,0)),...
		'_hes',sprintf('%03d',round(sum(Input.stationary_storage)/Input.entities*100,0)),...
		'_ev',sprintf('%03d',round(sum(Input.electric_vehicle)/Input.entities*100,0)),...
		];

	if isfile([Input.path_results,Input.scenario_name,'.mat'])
		error('File already exists.');
	end
	
	config_msg = ['Simulation ',Input.path_results,Input.scenario_name,' started.'];
	disp(config_msg); try fid = fopen(fullfile('log.txt'),'a'); fprintf(fid,'%s: %s\n',datestr(now,'yyyy-mm-dd HH:MM:SS'),config_msg); fclose(fid); clear fid; catch; end

	run('script_10_initialization.m'); % data initialization

	run('script_15_market_data.m'); % data creation for market data

	run('script_16_entity_data.m'); % data creation for entities

%% add the path of the gurobi solver, if available
	if Input.gurobi_solver
		gurobipath = fileparts(which('gurobi'));
		if ~isempty(gurobipath)
			addpath([gurobipath(1:end-6),'examples\matlab']);
		else			
			warning('The Gurobi solver was not found in the MATLAB environment.');
		end
	end
	clear gurobipath; % clear unused variables

%% run ems optimizations (unidirectional)
	if Input.optimization_full
		[EMS_uni_reference,EMS_uni_reference_full] = script_20_ems(Input,'reference'); % reference ems (without local market)
		if Input.entities == 1 || sum(Input.pv_system) == 0
			EMS_uni_decentral = EMS_uni_reference;
			EMS_uni_decentral_full = EMS_uni_reference_full;
			EMS_uni_central = EMS_uni_reference;
			EMS_uni_central_full = EMS_uni_reference_full;
		else
			[EMS_uni_decentral,EMS_uni_decentral_full] = script_20_ems(Input,'decentral'); % decentral ems + central clearing
			[EMS_uni_central,EMS_uni_central_full] = script_20_ems(Input,'central'); % central ems + clearing
		end
	else
		EMS_uni_reference = script_20_ems(Input,'reference'); % reference ems (without local market)
		if Input.entities == 1 || sum(Input.pv_system) == 0
			EMS_uni_decentral = EMS_uni_reference;
			EMS_uni_central = EMS_uni_reference;
		else
			EMS_uni_decentral = script_20_ems(Input,'decentral'); % decentral ems + central clearing
			EMS_uni_central = script_20_ems(Input,'central'); % central ems + clearing
		end
	end

%% run ems optimizations (bidirectional)
Input.EV.power_active_discharge = min(1.50*Input.EV.energy_nominal/1,Input.EV.power_active); % [kW] maximum active power during charging of ESS

	if Input.optimization_full
		if sum(Input.electric_vehicle) == 0
			EMS_bid_reference = EMS_uni_reference;
			EMS_bid_decentral = EMS_uni_decentral;
			EMS_bid_central = EMS_uni_central;
			EMS_bid_reference_full = EMS_uni_reference_full;
			EMS_bid_decentral_full = EMS_uni_decentral_full;
			EMS_bid_central_full = EMS_uni_central_full;
		else
			[EMS_bid_reference,EMS_bid_reference_full] = script_20_ems(Input,'reference'); % reference ems (without local market)
			if Input.entities == 1 || sum(Input.pv_system) == 0
				EMS_bid_decentral = EMS_bid_reference;
				EMS_bid_decentral_full = EMS_bid_reference_full;
				EMS_bid_central = EMS_bid_reference;
				EMS_bid_central_full = EMS_bid_reference_full;
			else
				[EMS_bid_decentral,EMS_bid_decentral_full] = script_20_ems(Input,'decentral'); % decentral ems + central clearing
				[EMS_bid_central,EMS_bid_central_full] = script_20_ems(Input,'central'); % central ems + clearing
			end
		end
	else
		if sum(Input.electric_vehicle) == 0
			EMS_bid_reference = EMS_uni_reference;
			EMS_bid_decentral = EMS_uni_decentral;
			EMS_bid_central = EMS_uni_central;
		else
			EMS_bid_reference = script_20_ems(Input,'reference'); % reference ems (without local market)
			if Input.entities == 1 || sum(Input.pv_system) == 0
				EMS_bid_decentral = EMS_bid_reference;
				EMS_bid_central = EMS_bid_reference;
			else
				EMS_bid_decentral = script_20_ems(Input,'decentral'); % decentral ems + central clearing
				EMS_bid_central = script_20_ems(Input,'central'); % central ems + clearing
			end
		end
	end

%% post calculation
	run('script_50_evaluation.m'); % evaluation

	config_msg = ['Simulation ',Input.path_results,Input.scenario_name,' was successful.'];
	disp(config_msg); try fid = fopen(fullfile('log.txt'),'a'); fprintf(fid,'%s: %s\n',datestr(now,'yyyy-mm-dd HH:MM:SS'),config_msg); fclose(fid); clear fid; catch; end

%% clean-up
	delete('*.asv');
