%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

function [EMS_concat,EMS_full] = script_20_ems(Input,ems_type)

	arguments
		Input struct
        ems_type {mustBeMember(ems_type,{'reference','decentral','central'})}
	end

for time_step = uint32(1:Input.simulation_time/Input.rolling_horizon)

%% variables
	if time_step == 1
		state_of_health_sess				= Input.SESS.state_of_health_initial;
		state_of_health_ev					= Input.EV.state_of_health_initial;
		energy_sess_actual_initial			= Input.SESS.energy_nominal .* state_of_health_sess .* Input.SESS.state_of_charge_initial;
		energy_ev_actual_initial			= Input.EV.energy_nominal .* state_of_health_ev .* Input.EV.state_of_charge_initial;
	else
		state_of_health_sess				= EMS_concat.Variables.state_of_health_sess(end,:);
		state_of_health_ev					= EMS_concat.Variables.state_of_health_ev(end,:);
	end
	price_purchase							= Input.price_purchase((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	price_sell_green						= Input.price_sell_green((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	price_sell_grey							= Input.price_sell_grey((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	price_trade_green_sell					= Input.price_trade_green_sell((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	price_trade_green_purchase				= Input.price_trade_green_purchase((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	price_trade_grey_sell					= Input.price_trade_grey_sell((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	price_trade_grey_purchase				= Input.price_trade_grey_purchase((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time)';
	power_load								= Input.power_load((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	power_pv_green							= Input.power_pv_green((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	power_pv_grey							= Input.power_pv_grey((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	temperature_cell_sess					= Input.SESS.temperature_cell((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	temperature_cell_ev						= Input.EV.temperature_cell((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	if Input.entities == 1
		power_load_prediction				= transpose(Input.power_load_prediction(time_step,:));
		power_pv_green_prediction			= transpose(Input.power_pv_green_prediction(time_step,:));
		power_pv_grey_prediction			= transpose(Input.power_pv_grey_prediction(time_step,:));
		power_ev_drive_prediction			= transpose(Input.power_ev_drive_prediction(time_step,:));
		ev_plugged_home_prediction			= transpose(Input.ev_plugged_home_prediction(time_step,:));
	else
		power_load_prediction				= squeeze(Input.power_load_prediction(time_step,:,:));
		power_pv_green_prediction			= squeeze(Input.power_pv_green_prediction(time_step,:,:));
		power_pv_grey_prediction			= squeeze(Input.power_pv_grey_prediction(time_step,:,:));
		power_ev_drive_prediction			= squeeze(Input.power_ev_drive_prediction(time_step,:,:));
		ev_plugged_home_prediction			= squeeze(Input.ev_plugged_home_prediction(time_step,:,:));
	end
	power_ev_drive							= Input.power_ev_drive((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	ev_plugged_home							= Input.ev_plugged_home((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);

%% update power profiles considering the previous clearing
	if time_step == 1
		power_local_supply_green_previous	= zeros(Input.optimization_time/Input.sample_time,Input.entities);
		power_local_supply_grey_previous	= zeros(Input.optimization_time/Input.sample_time,Input.entities);
		power_local_demand_green_previous	= zeros(Input.optimization_time/Input.sample_time,Input.entities);
		power_local_demand_grey_previous	= zeros(Input.optimization_time/Input.sample_time,Input.entities);
	else
		power_local_supply_green_previous	= circshift(EMS.Variables.power_local_supply_green_previous,-Input.rolling_horizon/Input.sample_time);
		power_local_supply_green_previous((end-Input.rolling_horizon/Input.sample_time+1:end),:)	= 0;

		power_local_supply_grey_previous	= circshift(EMS.Variables.power_local_supply_grey_previous,-Input.rolling_horizon/Input.sample_time);
		power_local_supply_grey_previous((end-Input.rolling_horizon/Input.sample_time+1:end),:)	= 0;

		power_local_demand_green_previous	= circshift(EMS.Variables.power_local_demand_green_previous,-Input.rolling_horizon/Input.sample_time);
		power_local_demand_green_previous((end-Input.rolling_horizon/Input.sample_time+1:end),:)	= 0;

		power_local_demand_grey_previous	= circshift(EMS.Variables.power_local_demand_grey_previous,-Input.rolling_horizon/Input.sample_time);
		power_local_demand_grey_previous((end-Input.rolling_horizon/Input.sample_time+1:end),:)	= 0;
	end
	power_load_adjusted					= Input.power_load((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) + power_local_supply_green_previous + power_local_supply_grey_previous;
	power_pv_green_adjusted				= Input.power_pv_green((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) + power_local_demand_green_previous;
	power_pv_grey_adjusted				= Input.power_pv_grey((time_step-1)*(Input.rolling_horizon/Input.sample_time)+1:(time_step-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) + power_local_demand_grey_previous;
	if Input.entities == 1
		power_load_prediction_adjusted		= transpose(Input.power_load_prediction(time_step,:)) + power_local_supply_green_previous + power_local_supply_grey_previous;
		power_pv_green_prediction_adjusted	= transpose(Input.power_pv_green_prediction(time_step,:)) + power_local_demand_green_previous;
		power_pv_grey_prediction_adjusted	= transpose(Input.power_pv_grey_prediction(time_step,:)) + power_local_demand_grey_previous;
	else
		power_load_prediction_adjusted		= squeeze(Input.power_load_prediction(time_step,:,:)) + power_local_supply_green_previous + power_local_supply_grey_previous;
		power_pv_green_prediction_adjusted	= squeeze(Input.power_pv_green_prediction(time_step,:,:)) + power_local_demand_green_previous;
		power_pv_grey_prediction_adjusted	= squeeze(Input.power_pv_grey_prediction(time_step,:,:)) + power_local_demand_grey_previous;
	end
	clear EMS;

%% create optimization problem
%% decision variables
		energy_sess_actual				= optimvar('energy_sess_actual',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',repmat(Input.SESS.energy_nominal.*Input.SESS.state_of_charge_min,Input.optimization_time/Input.sample_time,1),'UpperBound',repmat(Input.SESS.energy_nominal.*(state_of_health_sess-Input.stationary_storage+Input.SESS.state_of_charge_max),Input.optimization_time/Input.sample_time,1)); % [kWh] actual energy content
		energy_ev_actual				= optimvar('energy_ev_actual',				Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',repmat(Input.EV.energy_nominal.*Input.EV.state_of_charge_min,Input.optimization_time/Input.sample_time,1),'UpperBound',repmat(Input.EV.energy_nominal.*(state_of_health_ev-Input.electric_vehicle+Input.EV.state_of_charge_max),Input.optimization_time/Input.sample_time,1)); % [kWh] actual energy content
		energy_sess_charge				= optimvar('energy_sess_charge',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat((Input.SESS.power_active_charge*Input.sample_time),Input.optimization_time/Input.sample_time,1)); % [kWh] charging energy
		energy_ev_charge				= optimvar('energy_ev_charge',				Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',ev_plugged_home_prediction.*Input.EV.power_active_charge*Input.sample_time); % [kWh] charging energy
		energy_ev_charge_extern			= optimvar('energy_ev_charge_extern',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kWh] external charging energy
		energy_sess_discharge			= optimvar('energy_sess_discharge',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat((Input.SESS.power_active_discharge*Input.sample_time),Input.optimization_time/Input.sample_time,1)); % [kWh] discharging energy
		energy_ev_discharge				= optimvar('energy_ev_discharge',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',ev_plugged_home_prediction.*Input.EV.power_active_discharge*Input.sample_time); % [kWh] discharging energy
		energy_curtailment				= optimvar('energy_curtailment',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kWh] curtailment energy at btm node
		energy_buffer_ev				= optimvar('energy_buffer_ev',				Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kWh] buffer energy of electric vehicle for soc preference
		power_selfconsumed				= optimvar('power_selfconsumed',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] consumed pv power
		power_supply_green				= optimvar('power_supply_green',			Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.feedin_limit,Input.optimization_time/Input.sample_time,1)); % [kW] sold power from pv
		power_supply_grey				= optimvar('power_supply_grey',				Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] sold power from sess
		power_demand_grey				= optimvar('power_demand_grey',				Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] purchased power from grid
		cost_cycle_sess					= optimvar('cost_cycle_sess',				Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [EUR] cost due to cycle degradation at sess
		cost_cycle_ev					= optimvar('cost_cycle_ev',					Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [EUR] cost due to cycle degradation at ev
	if ~strcmp(ems_type,'decentral')
		power_global_demand_grey		= optimvar('power_global_demand_grey',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] purchased grey power on global market
		power_global_supply_green		= optimvar('power_global_supply_green',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] sold green power on global market
		power_global_supply_grey		= optimvar('power_global_supply_grey',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] sold grey power on global market
	end
	if strcmp(ems_type,'reference')
		power_local_demand_green		= optimvar('power_local_demand_green',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',0); % [kW] purchased green power on local market
		power_local_demand_grey			= optimvar('power_local_demand_grey',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',0); % [kW] purchased grey power on local market
		power_local_supply_green		= optimvar('power_local_supply_green',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',0); % [kW] sold green power on local market
		power_local_supply_grey			= optimvar('power_local_supply_grey',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',0); % [kW] sold power grey power on local market
	elseif strcmp(ems_type,'central')
		power_local_demand_green		= optimvar('power_local_demand_green',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] purchased green power on local market
		power_local_demand_grey			= optimvar('power_local_demand_grey',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] purchased grey power on local market
		power_local_supply_green		= optimvar('power_local_supply_green',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] sold green power on local market
		power_local_supply_grey			= optimvar('power_local_supply_grey',		Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0); % [kW] sold power grey power on local market
	end

%% objective function
	if strcmp(ems_type,'decentral')
		opt_objective_cost_energy = sum(sum( ...
			- repmat(price_sell_green,1,Input.entities) .* power_supply_green * Input.sample_time ...
			- repmat(price_sell_grey,1,Input.entities) .* power_supply_grey * Input.sample_time ...
			+ repmat(price_purchase,1,Input.entities) .* power_demand_grey * Input.sample_time ));
	else
		opt_objective_cost_energy_global = sum(sum( ...
			- repmat(price_sell_green,1,Input.entities) .* power_global_supply_green * Input.sample_time ...
			- repmat(price_sell_grey,1,Input.entities) .* power_global_supply_grey * Input.sample_time ...
			+ repmat(price_purchase,1,Input.entities) .* power_global_demand_grey * Input.sample_time ));

		opt_objective_cost_energy_local = sum(sum( ...
			- repmat(price_trade_green_sell,1,Input.entities) .* power_local_supply_green * Input.sample_time ...
			+ repmat(price_trade_green_purchase+1e-4,1,Input.entities) .* power_local_demand_green * Input.sample_time ...
			- repmat(price_trade_grey_sell,1,Input.entities) .* power_local_supply_grey * Input.sample_time ...
			+ repmat(price_trade_grey_purchase+1e-4,1,Input.entities) .* power_local_demand_grey * Input.sample_time ));
	end

		opt_objective_cost_cycle = sum( ...
			+ sum(cost_cycle_sess) ...
			+ sum(cost_cycle_ev) );

		opt_objective_end_soc = sum( ...
            - sum(energy_sess_actual .* 1e-8) ...
			- sum(energy_ev_actual .* 1e-8) );

		opt_objective_energy_buffer = ...
			+ 100 * sum(sum( energy_buffer_ev ));

		opt_objective_charge_extern = ...
			+ 999 * sum(sum( energy_ev_charge_extern ));

	if strcmp(ems_type,'reference') || strcmp(ems_type,'central')
		opt_objective = opt_objective_cost_energy_global + opt_objective_cost_energy_local ...
			+ opt_objective_cost_cycle + opt_objective_end_soc + opt_objective_energy_buffer + opt_objective_charge_extern;
	elseif strcmp(ems_type,'decentral')
		opt_objective = opt_objective_cost_energy ...
			+ opt_objective_cost_cycle + opt_objective_end_soc + opt_objective_energy_buffer + opt_objective_charge_extern;
	end

	opt_problem = optimproblem('Objective',opt_objective);

%% call constraints
	if strcmp(ems_type,'reference')
		opt_problem.Constraints.household_supply_green = ... % power balance at household
			power_supply_green == power_global_supply_green;
		opt_problem.Constraints.household_supply_grey = ...
			power_supply_grey == power_global_supply_grey;
		opt_problem.Constraints.household_demand_grey = ...
			power_demand_grey == power_global_demand_grey;
	elseif strcmp(ems_type,'central')
		opt_problem.Constraints.household_supply_green = ... % power balance at household
			power_supply_green == power_local_supply_green + power_global_supply_green;
		opt_problem.Constraints.household_supply_grey = ...
			power_supply_grey == power_local_supply_grey + power_global_supply_grey;
		opt_problem.Constraints.household_demand_grey = ...
			power_demand_grey == power_local_demand_grey + power_local_demand_green + power_global_demand_grey;

		opt_problem.Constraints.local_green = ... % power balance at local market
			sum(power_local_supply_green,2) == sum(power_local_demand_green,2);
		opt_problem.Constraints.local_grey = ...
			sum(power_local_supply_grey,2) == sum(power_local_demand_grey,2);

		opt_problem.Constraints.surpress_offers_1 = ... % surpress local offers outside trading horizon
			power_local_supply_green(Input.trading_horizon/Input.sample_time+1:end,:) == 0;
		opt_problem.Constraints.surpress_offers_2 = ...
			power_local_supply_grey(Input.trading_horizon/Input.sample_time+1:end,:) == 0;
		opt_problem.Constraints.surpress_offers_3 = ...
			power_local_demand_green(Input.trading_horizon/Input.sample_time+1:end,:) == 0;
		opt_problem.Constraints.surpress_offers_4 = ...
			power_local_demand_grey(Input.trading_horizon/Input.sample_time+1:end,:) == 0;

% 		opt_problem.Constraints.electricity_costs = ...
% 			sum(+ repmat(price_purchase,1,Input.entities) .* power_load_prediction_adjusted * Input.sample_time ...
% 			+ repmat(price_purchase,1,Input.entities) .* power_ev_drive_prediction / 0.894 * Input.sample_time ...
% 			- repmat(price_sell_grey,1,Input.entities) .* power_pv_grey_prediction_adjusted * Input.sample_time ...
% 			- repmat(price_sell_green,1,Input.entities) .* power_pv_green_prediction_adjusted * Input.sample_time,1) ...
% 			<= ...
% 			sum(+ repmat(price_purchase,1,Input.entities) .* power_global_demand_grey * Input.sample_time ...
% 			- repmat(price_sell_green,1,Input.entities) .* power_global_supply_green * Input.sample_time ...
% 			- repmat(price_sell_grey,1,Input.entities) .* power_global_supply_grey * Input.sample_time ...
% 			+ repmat(price_trade_green_purchase+1e-4,1,Input.entities) .* power_local_demand_green * Input.sample_time ...
% 			+ repmat(price_trade_grey_purchase+1e-4,1,Input.entities) .* power_local_demand_grey * Input.sample_time ...
% 			- repmat(price_trade_green_sell,1,Input.entities) .* power_local_supply_green * Input.sample_time ...
% 			- repmat(price_trade_grey_sell,1,Input.entities) .* power_local_supply_grey * Input.sample_time,1);
	end

	run('script_25_entity_constraints.m');

%% solve optimization
% define solver options
	if strcmp(Input.algorithm,'intlinprog')
		opt_options = optimoptions('intlinprog','Display','none','MaxTime',Input.max_optimization_time,'RelativeGapTolerance',Input.max_relative_optimization_gap);
	elseif strcmp(Input.algorithm,'linprog')
		opt_options = optimoptions('linprog','Display','none','MaxTime',Input.max_optimization_time,'OptimalityTolerance',Input.max_relative_optimization_gap);
	end

% call solver
	[opt_variables,opt_objective,opt_flag,opt_settings] = solve(opt_problem,'Options',opt_options);

% check optimization success
	if opt_flag ~= 1; error([upper(ems_type(1)),ems_type(2:end),' EMS optimization was not successful']); end

%% create optimization container
	EMS.Problem		= opt_problem;
	EMS.Options		= opt_options;
	EMS.Settings	= opt_settings;
	EMS.Variables	= opt_variables;
	EMS.Objective	= opt_objective;
	EMS.ExitFlag	= opt_flag;

%% add variables to optimization container
	EMS.Variables.price_purchase						= price_purchase;
	EMS.Variables.price_sell_green						= price_sell_green;
	EMS.Variables.price_sell_grey						= price_sell_grey;
	EMS.Variables.price_trade_green_sell				= price_trade_green_sell;
	EMS.Variables.price_trade_green_purchase			= price_trade_green_purchase;
	EMS.Variables.price_trade_grey_sell					= price_trade_grey_sell;
	EMS.Variables.price_trade_grey_purchase				= price_trade_grey_purchase;
	EMS.Variables.power_load							= power_load;
	EMS.Variables.power_pv_green						= power_pv_green;
	EMS.Variables.power_pv_grey							= power_pv_grey;
	EMS.Variables.power_load_prediction					= power_load_prediction;
	EMS.Variables.power_pv_green_prediction				= power_pv_green_prediction;
	EMS.Variables.power_pv_grey_prediction				= power_pv_grey_prediction;
	EMS.Variables.power_load_adjusted					= power_load_adjusted;
	EMS.Variables.power_pv_green_adjusted				= power_pv_green_adjusted;
	EMS.Variables.power_pv_grey_adjusted				= power_pv_grey_adjusted;
	EMS.Variables.power_load_prediction_adjusted		= power_load_prediction_adjusted;
	EMS.Variables.power_pv_green_prediction_adjusted	= power_pv_green_prediction_adjusted;
	EMS.Variables.power_pv_grey_prediction_adjusted		= power_pv_grey_prediction_adjusted;
	EMS.Variables.power_ev_drive						= power_ev_drive;
	EMS.Variables.ev_plugged_home						= ev_plugged_home;
	EMS.Variables.power_ev_drive_prediction				= power_ev_drive_prediction;
	EMS.Variables.ev_plugged_home_prediction			= ev_plugged_home_prediction;
	EMS.Variables.temperature_cell_sess					= temperature_cell_sess;
	EMS.Variables.temperature_cell_ev					= temperature_cell_ev;

%% run central clearing for decentral ems optimizations
	if strcmp(ems_type,'decentral')
		EMS = script_26_decentral_clearing(Input,EMS);
	end

%% run matching for trades between peers
	EMS = script_27_matching(Input,EMS);

	EMS.Variables.power_local_supply_green_previous		= power_local_supply_green_previous + EMS.Variables.power_local_supply_green;
	EMS.Variables.power_local_supply_grey_previous		= power_local_supply_grey_previous + EMS.Variables.power_local_supply_grey;
	EMS.Variables.power_local_demand_green_previous		= power_local_demand_green_previous + EMS.Variables.power_local_demand_green;
	EMS.Variables.power_local_demand_grey_previous		= power_local_demand_grey_previous + EMS.Variables.power_local_demand_grey;

	% clear unused variables
	clear opt* energy* power* price* ev* cost*;

%% correction of decision variables (vectors with uniform dimensions)
	EMS.Variables = orderfields(EMS.Variables);
	fields = fieldnames(EMS.Variables);
	for i = uint32(1:length(fields))
		field = ['EMS.Variables.',fields{i}];
		field_dummy = eval(field);
		if size(field_dummy,1) ~= Input.optimization_time/Input.sample_time
			eval([field,' = repelem(',field,',(Input.optimization_time/Input.sample_time)/length(field_dummy),:,:);']);
		end
	end
	clear field* i;

%% save all optimization results
	if Input.optimization_full && Input.simulation_time/Input.rolling_horizon > 1
		EMS_full(time_step) = EMS;
	end

%% save only data during the rolling horizon
	EMS_short = EMS;
	fields = fieldnames(EMS_short.Variables);
	for i = uint32(1:length(fields))
		EMS_short.Variables.(fields{i})((Input.rolling_horizon/Input.sample_time)+1:end,:,:) = [];
	end
	clear field* i;

%% call script for the state of health calculation
	run('script_29_soh_calculation.m');
	clear state*

%% concatenate results from optimizations
	EMS_concat.Problem				= EMS_short.Problem;
	EMS_concat.Options				= EMS_short.Options;
	EMS_concat.Settings(time_step)	= EMS_short.Settings;
	fields = fieldnames(EMS_short.Variables);
	for i = uint32(1:length(fields))
		if time_step == 1
			EMS_concat.Variables.(fields{i})	= EMS_short.Variables.(fields{i});
		else
			EMS_concat.Variables.(fields{i})	= [EMS_concat.Variables.(fields{i});EMS_short.Variables.(fields{i})];
		end
	end
	EMS_concat.Objective(time_step)	= EMS_short.Objective;
	EMS_concat.ExitFlag(time_step)	= EMS_short.ExitFlag;
	EMS_concat.matching.power_local_trade_green(time_step,:,:,:) = EMS.Variables.power_local_trade_green;
	EMS_concat.matching.power_local_trade_grey(time_step,:,:,:) = EMS.Variables.power_local_trade_grey;
	clear EMS_short field* i;

%% show optimization progress	
	disp(['[ ',num2str(time_step),' / ',num2str(round(Input.simulation_time/Input.rolling_horizon,0)),' ] ',upper(ems_type(1)),ems_type(2:end),' EMS optimization completed',newline]);

end

%% correction of decision variables (convert to single)
	EMS_concat.Variables = orderfields(EMS_concat.Variables);
	fields = fieldnames(EMS_concat.Variables);
	for i = uint32(1:length(fields))
		EMS_concat.Variables.(fields{i}) = single(EMS_concat.Variables.(fields{i}));
	end
	clear field* i;

% update arrays
    EMS_concat = EMS_concat.Variables;
	EMS_concat.power_local_demand_green	= EMS_concat.power_local_demand_green_previous;
	EMS_concat.power_local_demand_grey	= EMS_concat.power_local_demand_grey_previous;
	EMS_concat.power_local_supply_green	= EMS_concat.power_local_supply_green_previous;
	EMS_concat.power_local_supply_grey	= EMS_concat.power_local_supply_grey_previous;

% delete unused fields
	EMS_concat = rmfield(EMS_concat,{'power_ev_drive_prediction',...
		'ev_plugged_home_prediction','power_load_adjusted','power_load_prediction',...
		'power_pv_green_adjusted','power_pv_green_prediction','power_pv_grey_adjusted',...
		'power_pv_grey_prediction'});
	EMS_concat = rmfield(EMS_concat,{'cost_cycle_ev','cost_cycle_sess','energy_buffer_ev','power_load_prediction_adjusted',...
		'power_local_demand_green_previous','power_local_demand_grey_previous','power_local_supply_green_previous',...
		'power_local_supply_grey_previous','power_pv_green_prediction_adjusted','power_pv_grey_prediction_adjusted',...
		'power_global_demand_grey','power_global_supply_green','power_global_supply_grey','power_selfconsumed',...
		'price_purchase','price_sell_green','price_sell_grey','price_trade_green_purchase','price_trade_green_sell',...
		'price_trade_grey_purchase','price_trade_grey_sell','capacity_loss_ev_total','capacity_loss_sess_total'});
	EMS_concat = orderfields(EMS_concat);

% conduct degradation for longer time period
	calculated_years = 30;
	for entity = 1:Input.entities
		if Input.SESS.energy_nominal(entity) ~= 0
			state_of_charge = EMS_concat.energy_sess_actual(:,entity) ./ EMS_concat.state_of_health_sess(:,entity) ./ Input.SESS.energy_nominal(entity);
				state_of_charge(isnan(state_of_charge)) = 0;
				state_of_charge = repmat(state_of_charge,calculated_years,1);
			temperature = repmat(EMS_concat.temperature_cell_sess,calculated_years,1);
			[Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_lfp(state_of_charge,temperature,0,0,0.25);
			state_of_health = 1-cumsum(Q_loss_calendar+Q_loss_cycle);
		else
			state_of_health = zeros(size(EMS_concat.energy_sess_actual,1)*calculated_years,1);
		end
		EMS_concat.soh_sess(:,entity) = state_of_health(1:(1/Input.sample_time)*24:end);
			clear state_of_charge temperature Q_loss* state_of_health;
		if Input.EV.energy_nominal(entity) ~= 0
			state_of_charge = EMS_concat.energy_ev_actual(:,entity) ./ EMS_concat.state_of_health_ev(:,entity) ./ Input.EV.energy_nominal(entity);
				state_of_charge(isnan(state_of_charge)) = 0;
				state_of_charge = repmat(state_of_charge,calculated_years,1);
			temperature = repmat(EMS_concat.temperature_cell_ev,calculated_years,1);
			[Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_nmc(state_of_charge,temperature,0.25/24:0.25/24:365*calculated_years,0,0,2.05);
			state_of_health = 1-cumsum(Q_loss_calendar+Q_loss_cycle);
		else
			state_of_health = zeros(size(EMS_concat.energy_ev_actual,1)*calculated_years,1);
		end
		EMS_concat.soh_ev(:,entity) = state_of_health(1:(1/Input.sample_time)*24:end);
			clear state_of_charge temperature Q_loss* state_of_health;
	end
	EMS_concat = rmfield(EMS_concat,{'state_of_health_sess','state_of_health_ev'});
	EMS_concat.state_of_health_sess = EMS_concat.soh_sess;
	EMS_concat.state_of_health_ev = EMS_concat.soh_ev;
	EMS_concat = rmfield(EMS_concat,{'soh_sess','soh_ev'});

end