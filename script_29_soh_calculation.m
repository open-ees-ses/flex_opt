%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

%% state of charge calculation
for entity = 1:Input.entities
	if Input.soh_caluclation && Input.stationary_storage(entity)==1
		state_of_charge_sess = EMS_short.Variables.energy_sess_actual(:,entity) ./ Input.SESS.energy_nominal(entity);
			state_of_charge_sess(isnan(state_of_charge_sess)) = 0;
		temperature_sess = temperature_cell_sess;
		if strcmp(Input.SESS.cell_chemistry(entity),'LFP')
			if time_step == 1
				capacity_loss_sess_total = 1-state_of_health_sess(entity);
				capacity_loss_sess_calendar = capacity_loss_sess_total * 2/3;
			else
				capacity_loss_sess_total = sum(EMS_concat.Variables.capacity_loss_sess_total(:,entity));
				capacity_loss_sess_calendar = sum(EMS_concat.Variables.capacity_loss_sess_calendar(:,entity));				
			end
			[Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_lfp(...
				state_of_charge_sess,...
				temperature_sess,...
				capacity_loss_sess_total,...
				capacity_loss_sess_calendar,...
				Input.sample_time);
			EMS_short.Variables.capacity_loss_sess_calendar(:,entity) = Q_loss_calendar;
			EMS_short.Variables.capacity_loss_sess_cycle(:,entity) = Q_loss_cycle;
			clear Q_loss*;
		elseif strcmp(Input.SESS.cell_chemistry(entity),'NMC')
			time = (1+double(time_step-1)*(Input.rolling_horizon/Input.sample_time):1:double(time_step)*(Input.rolling_horizon/Input.sample_time))' * Input.sample_time / 24;
			time_previous = (double(time_step)-1)*(Input.rolling_horizon/Input.sample_time) * Input.sample_time / 24;
			if time_step == 1
				charge_throughput_previous = 0;
			else
				state_of_charge = EMS_concat.Variables.energy_sess_actual(:,entity) ./ Input.SESS.energy_nominal(entity); state_of_charge(isnan(state_of_charge)) = 0;
				depth_of_discharge = abs([0;diff(state_of_charge)]);
				charge_throughput_previous = sum(Input.SESS.capacity_cell_nominal(entity) * depth_of_discharge);
			end
			[Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_nmc(...
				state_of_charge_sess,...
				temperature_sess,...
				time,...
				time_previous,...
				charge_throughput_previous,...
				Input.SESS.capacity_cell_nominal(entity));
			EMS_short.Variables.capacity_loss_sess_calendar(:,entity) = Q_loss_calendar;
			EMS_short.Variables.capacity_loss_sess_cycle(:,entity) = Q_loss_cycle;
			clear Q_loss*;
		end
	else
		EMS_short.Variables.capacity_loss_sess_calendar(:,entity) = zeros(Input.rolling_horizon/Input.sample_time,1);
		EMS_short.Variables.capacity_loss_sess_cycle(:,entity) = zeros(Input.rolling_horizon/Input.sample_time,1);		
	end

	if Input.soh_caluclation && Input.electric_vehicle(entity)==1
		state_of_charge_ev = EMS_short.Variables.energy_ev_actual(:,entity) ./ Input.EV.energy_nominal(entity);
			state_of_charge_ev(isnan(state_of_charge_ev)) = 0;
		temperature_ev = temperature_cell_ev;
		if strcmp(Input.EV.cell_chemistry(entity),'LFP')
			if time_step == 1
				capacity_loss_ev_total = 1-state_of_health_ev(entity);
				capacity_loss_ev_calendar = capacity_loss_ev_total * 2/3;
			else
				capacity_loss_ev_total = sum(EMS_concat.Variables.capacity_loss_ev_total(:,entity));
				capacity_loss_ev_calendar = sum(EMS_concat.Variables.capacity_loss_ev_calendar(:,entity));				
			end
			[Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_lfp(...
				state_of_charge_ev,...
				temperature_ev,...
				capacity_loss_ev_total,...
				capacity_loss_ev_calendar,...
				Input.sample_time);
			EMS_short.Variables.capacity_loss_ev_calendar(:,entity) = Q_loss_calendar;
			EMS_short.Variables.capacity_loss_ev_cycle(:,entity) = Q_loss_cycle;
			clear Q_loss*;
		elseif strcmp(Input.EV.cell_chemistry(entity),'NMC')
			time = (1+double(time_step-1)*(Input.rolling_horizon/Input.sample_time):1:double(time_step)*(Input.rolling_horizon/Input.sample_time))' * Input.sample_time / 24;
			time_previous = (double(time_step)-1)*(Input.rolling_horizon/Input.sample_time) * Input.sample_time / 24;
			if time_step == 1
				charge_throughput_previous = 0;
			else
				state_of_charge = EMS_concat.Variables.energy_ev_actual(:,entity) ./ Input.EV.energy_nominal(entity); state_of_charge(isnan(state_of_charge)) = 0;
				depth_of_discharge = abs([0;diff(state_of_charge)]);
				charge_throughput_previous = sum(Input.EV.capacity_cell_nominal(entity) * depth_of_discharge);
			end			
			[Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_nmc(...
				state_of_charge_ev,...
				temperature_ev,...
				time,...
				time_previous,...
				charge_throughput_previous,...
				Input.EV.capacity_cell_nominal(entity));
			EMS_short.Variables.capacity_loss_ev_calendar(:,entity) = Q_loss_calendar;
			EMS_short.Variables.capacity_loss_ev_cycle(:,entity) = Q_loss_cycle;
			clear Q_loss*;
		end
	else
		EMS_short.Variables.capacity_loss_ev_calendar(:,entity) = zeros(Input.rolling_horizon/Input.sample_time,1);
		EMS_short.Variables.capacity_loss_ev_cycle(:,entity) = zeros(Input.rolling_horizon/Input.sample_time,1);		
	end
end
clear entity state_of_charge* temperature* time charge_throughput_previous capacity_loss* depth_of_discharge;

%%
% superposition of calendar and cycle degradation
	EMS_short.Variables.capacity_loss_sess_total = EMS_short.Variables.capacity_loss_sess_calendar + EMS_short.Variables.capacity_loss_sess_cycle;
	EMS_short.Variables.capacity_loss_ev_total = EMS_short.Variables.capacity_loss_ev_calendar + EMS_short.Variables.capacity_loss_ev_cycle;

% state of health calculation
	EMS_short.Variables.state_of_health_sess = repmat(state_of_health_sess,Input.rolling_horizon/Input.sample_time,1) - cumsum(EMS_short.Variables.capacity_loss_sess_total,1);
	EMS_short.Variables.state_of_health_ev = repmat(state_of_health_ev,Input.rolling_horizon/Input.sample_time,1) - cumsum(EMS_short.Variables.capacity_loss_ev_total,1);
	
clear state_of_health*;