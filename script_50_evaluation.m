%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

%% drop unused data
	if isfield(Input,'price_trade_green')
		Input = rmfield(Input,{'price_trade_green','price_trade_grey','prediction_discrepancy',...
			'prediction_discrepancy_load','prediction_discrepancy_pv','prediction_discrepancy_ev_drive',...
			'power_load','power_pv','power_pv_green','power_pv_grey','power_ev_drive','ev_plugged_home',...
			'power_load_prediction','power_pv_prediction','power_ev_drive_prediction',...
			'ev_plugged_home_prediction','power_pv_green_prediction','power_pv_grey_prediction'...
			'profile_length','price_grid_charges','price_distribution','price_surcharges'});
		Input.SESS = rmfield(Input.SESS,{'temperature_cell'});
		Input.EV = rmfield(Input.EV,{'temperature_cell'});

		Fields = fieldnames(Input);
		for j = 1:length(Fields)
			Field = ['Input.',Fields{j}];
			if eval(['isa(',Field,',"struct")'])
				Fields2 = eval(['fieldnames(',Field,');']);
				for k = 1:length(Fields2)
					Field2 = [Field,'.',Fields2{k}];
					eval([Field,'_',Fields2{k},' = ',Field2,';']);
				end; clear k Fields2 Field2;
				Input = rmfield(Input,Fields{j});
			end
		end; clear j Fields Field;

		Input = orderfields(Input);
	else
		Input.plot_png = true;
	end

%% save results
	if Input.save_results
		if exist(Input.path_results,'dir') ~= 7; mkdir(Input.path_results); end
		save([Input.path_results,Input.scenario_name],'Input','EMS_uni_central','EMS_uni_decentral','EMS_uni_reference','EMS_bid_central','EMS_bid_decentral','EMS_bid_reference');
	end

%% plot data
if Input.plot_png || Input.plot_pdf
	close all;
	line_width = 1.3;
	set(0, 'DefaultLineLineWidth', line_width);
	font_name = 'Arial';
	font_size = 16;
	ax_border = [.07,0.01];
	ax_dist = .04;

% create sampling vector for the x-axis (time in days)
	plot_sampling = Input.simulation_start/(24*1)+Input.simulation_time/(24*1)/(Input.simulation_time/Input.sample_time): ...
		Input.simulation_time/(24*1)/(Input.simulation_time/Input.sample_time): ...
		Input.simulation_start/(24*1)+Input.simulation_time/(24*1);
	xlimtime = [Input.simulation_start/(24*1) (Input.simulation_start+Input.simulation_time)/(24*1)];

	figure('Units','normalized','Position',[.1 .1 .8 .8]); box on; hold on; grid on;
		title('Real Data');
		plot(plot_sampling,EMS_uni_reference.power_load(1:Input.simulation_time/Input.sample_time,:)+EMS_uni_reference.power_ev_drive(1:Input.simulation_time/Input.sample_time,:)-EMS_uni_reference.power_pv_green(1:Input.simulation_time/Input.sample_time,:)-EMS_uni_reference.power_pv_grey(1:Input.simulation_time/Input.sample_time,:));
		xlim(xlimtime);
		xlabel('Time / days'); ylabel('Residual Power / kW');
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		set(gca,'LooseInset',get(gca,'TightInset')); a = get(gca,'Position'); set(gca,'Position',[a(1) a(2) a(3)-0.002 a(4)-0.005]);
		ax=gca; ax_outerpos=ax.OuterPosition; ax_ti=ax.TightInset; ax_bottom=ax_outerpos(2)+ax_ti(2); ax_height=ax_outerpos(4)-ax_ti(2)-ax_ti(4);
		clear a;

	figure('Units','normalized','Position',[.1 .1 .8 .8]);
		ax1 = subplot(1,3,1); box on; hold on; grid on;
			for i=1:Input.entities
				if Input.stationary_storage(i)==1
					plot(plot_sampling,EMS_uni_reference.energy_sess_actual(:,i)./Input.SESS_energy_nominal(i)*100,'DisplayName',['Peer ',sprintf('%03.0f',uint32(i))]);
				end
			end
			xlim(xlimtime); ylim([0 100]);
			xlabel('Time / days'); ylabel('State of charge / %');
			text(mean(xlimtime),2.5,['EFC: [',num2str(round(min(sum(abs(diff(EMS_uni_reference.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),' , ', ...
				num2str(round(nanmean(sum(abs(diff(EMS_uni_reference.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),' , ', ...
				num2str(round(max(sum(abs(diff(EMS_uni_reference.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),']'], ...
				'HorizontalAlignment','center');
			title('SOC @ SESS (reference)');
			legend;
			ax=gca; ax_left=ax_border(1); ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax2 = subplot(1,3,2); box on; hold on; grid on;
			for i=1:Input.entities
				if Input.stationary_storage(i)==1
					plot(plot_sampling,EMS_uni_decentral.energy_sess_actual(:,i)./Input.SESS_energy_nominal(i)*100,'DisplayName',['Peer ',sprintf('%03.0f',uint32(i))]);
				end
			end
			xlim(xlimtime); ylim([0 100]);
			xlabel('Time / days'); ylabel('State of charge / %');
			text(mean(xlimtime),2.5,['EFC: [',num2str(round(min(sum(abs(diff(EMS_uni_decentral.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),' , ', ...
				num2str(round(nanmean(sum(abs(diff(EMS_uni_decentral.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),' , ', ...
				num2str(round(max(sum(abs(diff(EMS_uni_decentral.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),']'], ...
				'HorizontalAlignment','center');
			title('SOC @ SESS (decentral)');
			ax=gca; ax_left=ax_border(1)+1*ax_width+1*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax3 = subplot(1,3,3); box on; hold on; grid on;
			for i=1:Input.entities
				if Input.stationary_storage(i)==1
					plot(plot_sampling,EMS_uni_central.energy_sess_actual(:,i)./Input.SESS_energy_nominal(i)*100,'DisplayName',['Peer ',sprintf('%03.0f',uint32(i))]);
				end
			end
			xlim(xlimtime); ylim([0 100]);
			xlabel('Time / days'); ylabel('State of charge / %');
			text(mean(xlimtime),2.5,['EFC: [',num2str(round(min(sum(abs(diff(EMS_uni_central.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),' , ', ...
				num2str(round(nanmean(sum(abs(diff(EMS_uni_central.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),' , ', ...
				num2str(round(max(sum(abs(diff(EMS_uni_central.energy_sess_actual))./(2*Input.SESS_energy_nominal))),1)),']'], ...
				'HorizontalAlignment','center');
			title('SOC @ SESS (central)');
			ax=gca; ax_left=ax_border(1)+2*ax_width+2*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		linkaxes([ax1,ax2,ax3],'xy');

	figure('Units','normalized','Position',[.1 .1 .8 .8]);
		ax1 = subplot(1,3,1); box on; hold on; grid on;
			for i=1:Input.entities
				if Input.electric_vehicle(i)==1
					plot(plot_sampling,EMS_uni_reference.energy_ev_actual(:,i)./Input.EV_energy_nominal(i)*100,'DisplayName',['Peer ',sprintf('%03.0f',uint32(i))]);
				end
			end
			xlim(xlimtime); ylim([0 100]);
			xlabel('Time / days'); ylabel('State of charge / %');
			text(mean(xlimtime),2.5,['EFC: [',num2str(round(min(sum(abs(diff(EMS_uni_reference.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),' , ', ...
				num2str(round(nanmean(sum(abs(diff(EMS_uni_reference.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),' , ', ...
				num2str(round(max(sum(abs(diff(EMS_uni_reference.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),']'], ...
				'HorizontalAlignment','center');
			title('SOC @ EV (reference)');
			legend();
			ax=gca; ax_left=ax_border(1); ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax2 = subplot(1,3,2); box on; hold on; grid on;
			for i=1:Input.entities
				if Input.electric_vehicle(i)==1
					plot(plot_sampling,EMS_uni_decentral.energy_ev_actual(:,i)./Input.EV_energy_nominal(i)*100,'DisplayName',['Peer ',sprintf('%03.0f',uint32(i))]);
				end
			end
			xlim(xlimtime); ylim([0 100]);
			xlabel('Time / days'); ylabel('State of charge / %');
			text(mean(xlimtime),2.5,['EFC: [',num2str(round(min(sum(abs(diff(EMS_uni_decentral.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),' , ', ...
				num2str(round(nanmean(sum(abs(diff(EMS_uni_decentral.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),' , ', ...
				num2str(round(max(sum(abs(diff(EMS_uni_decentral.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),']'], ...
				'HorizontalAlignment','center');
			title('SOC @ EV (decentral)');
			ax=gca; ax_left=ax_border(1)+1*ax_width+1*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax3 = subplot(1,3,3); box on; hold on; grid on;
			for i=1:Input.entities
				if Input.electric_vehicle(i)==1
					plot(plot_sampling,EMS_uni_central.energy_ev_actual(:,i)./Input.EV_energy_nominal(i)*100,'DisplayName',['Peer ',sprintf('%03.0f',uint32(i))]);
				end
			end
			xlim(xlimtime); ylim([0 100]);
			xlabel('Time / days'); ylabel('State of charge / %');
			text(mean(xlimtime),2.5,['EFC: [',num2str(round(min(sum(abs(diff(EMS_uni_central.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),' , ', ...
				num2str(round(nanmean(sum(abs(diff(EMS_uni_central.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),' , ', ...
				num2str(round(max(sum(abs(diff(EMS_uni_central.energy_ev_actual))./(2*Input.EV_energy_nominal))),1)),']'], ...
				'HorizontalAlignment','center');
			title('SOC @ EV (central)');
			ax=gca; ax_left=ax_border(1)+2*ax_width+2*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		linkaxes([ax1,ax2,ax3],'xy');

	E_net_demand_reference = double((EMS_uni_reference.power_demand_grey-EMS_uni_reference.power_supply_grey-EMS_uni_reference.power_supply_green)*Input.sample_time);
	E_net_demand_reference(E_net_demand_reference<0) = 0; E_net_demand_reference = sum(E_net_demand_reference,2);
	E_net_supply_reference = double((EMS_uni_reference.power_demand_grey-EMS_uni_reference.power_supply_grey-EMS_uni_reference.power_supply_green)*Input.sample_time);
	E_net_supply_reference(E_net_supply_reference>0) = 0; E_net_supply_reference = sum(E_net_supply_reference,2);
	E_net_purchase_reference = double(E_net_demand_reference + E_net_supply_reference);
	E_net_demand_decentral = double((EMS_uni_decentral.power_demand_grey-EMS_uni_decentral.power_supply_grey-EMS_uni_decentral.power_supply_green)*Input.sample_time);
	E_net_demand_decentral(E_net_demand_decentral<0) = 0; E_net_demand_decentral = sum(E_net_demand_decentral,2);
	E_net_supply_decentral = double((EMS_uni_decentral.power_demand_grey-EMS_uni_decentral.power_supply_grey-EMS_uni_decentral.power_supply_green)*Input.sample_time);
	E_net_supply_decentral(E_net_supply_decentral>0) = 0; E_net_supply_decentral = sum(E_net_supply_decentral,2);
	E_net_purchase_decentral = double(E_net_demand_decentral + E_net_supply_decentral);
	E_net_demand_central = double((EMS_uni_central.power_demand_grey-EMS_uni_central.power_supply_grey-EMS_uni_central.power_supply_green)*Input.sample_time);
	E_net_demand_central(E_net_demand_central<0) = 0; E_net_demand_central = sum(E_net_demand_central,2);
	E_net_supply_central = double((EMS_uni_central.power_demand_grey-EMS_uni_central.power_supply_grey-EMS_uni_central.power_supply_green)*Input.sample_time);
	E_net_supply_central(E_net_supply_central>0) = 0; E_net_supply_central = sum(E_net_supply_central,2);
	E_net_purchase_central = double(E_net_demand_central + E_net_supply_central);
	figure('Units','normalized','Position',[.1 .1 .8 .8]);
		ax1 = subplot(1,3,1); box on; hold on; grid on;
% 			stairs(plot_sampling,E_net_purchase_reference/Input.sample_time,'LineWidth',line_width,'DisplayName','Net Energy Purchase');
			stairs(plot_sampling,E_net_demand_reference/Input.sample_time,'LineWidth',line_width,'Color',0.5+[.4 .0 .4],'DisplayName','Net Power Demand');
			stairs(plot_sampling,E_net_supply_reference/Input.sample_time,'LineWidth',line_width,'Color',0.5+[.0 .4 .4],'DisplayName','Net Power Supply');
			bar(plot_sampling+(plot_sampling(2)-plot_sampling(1))/2,sum([EMS_uni_reference.power_local_demand_green,EMS_uni_reference.power_local_demand_grey],2),1,'FaceColor',0.75*[.7 .0 .7],'DisplayName','Traded Demand');
			bar(plot_sampling+(plot_sampling(2)-plot_sampling(1))/2,-sum([EMS_uni_reference.power_local_supply_green,EMS_uni_reference.power_local_supply_grey],2),1,'FaceColor',0.75*[.0 .7 .7],'DisplayName','Traded Supply');
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Power / kW');
			legend;
			title('Reference case');
			ax=gca; ax_left=ax_border(1); ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax2 = subplot(1,3,2); box on; hold on; grid on;
% 			stairs(plot_sampling,E_net_purchase_decentral/Input.sample_time,'LineWidth',line_width,'DisplayName','Net Energy Purchase');
			stairs(plot_sampling,E_net_demand_decentral/Input.sample_time,'LineWidth',line_width,'Color',0.5+[.4 .0 .4],'DisplayName','Net Power Demand');
			stairs(plot_sampling,E_net_supply_decentral/Input.sample_time,'LineWidth',line_width,'Color',0.5+[.0 .4 .4],'DisplayName','Net Power Supply');
			bar(plot_sampling+(plot_sampling(2)-plot_sampling(1))/2,sum([EMS_uni_decentral.power_local_demand_green,EMS_uni_decentral.power_local_demand_grey],2),1,'FaceColor',0.75*[.7 .0 .7],'DisplayName','Traded Demand');
			bar(plot_sampling+(plot_sampling(2)-plot_sampling(1))/2,-sum([EMS_uni_decentral.power_local_supply_green,EMS_uni_decentral.power_local_supply_grey],2),1,'FaceColor',0.75*[.0 .7 .7],'DisplayName','Traded Supply');
			xlim(xlimtime);
			xlabel('Time / days'); ylabel([]);
			legend;
			title('Decentral P2P');
			ax=gca; ax_left=ax_border(1)+1*ax_width+1*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax3 = subplot(1,3,3); box on; hold on; grid on;
% 			stairs(plot_sampling,E_net_purchase_central/Input.sample_time,'LineWidth',line_width,'DisplayName','Net Energy Purchase');
			stairs(plot_sampling,E_net_demand_central/Input.sample_time,'LineWidth',line_width,'Color',0.5+[.4 .0 .4],'DisplayName','Net Power Demand');
			stairs(plot_sampling,E_net_supply_central/Input.sample_time,'LineWidth',line_width,'Color',0.5+[.0 .4 .4],'DisplayName','Net Power Supply');
			bar(plot_sampling+(plot_sampling(2)-plot_sampling(1))/2,sum([EMS_uni_central.power_local_demand_green,EMS_uni_central.power_local_demand_grey],2),1,'FaceColor',0.75*[.7 .0 .7],'DisplayName','Traded Demand');
			bar(plot_sampling+(plot_sampling(2)-plot_sampling(1))/2,-sum([EMS_uni_central.power_local_supply_green,EMS_uni_central.power_local_supply_grey],2),1,'FaceColor',0.75*[.0 .7 .7],'DisplayName','Traded Supply');
			xlim(xlimtime);
			xlabel('Time / days'); ylabel([]);
			legend;
			title('Central P2P');
			ax=gca; ax_left=ax_border(1)+2*ax_width+2*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		linkaxes([ax1,ax2,ax3],'xy');

	profit_local_reference = double(EMS_uni_reference.power_local_supply_green * Input.sample_time .* repmat(Input.price_trade_green_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		+ EMS_uni_reference.power_local_supply_grey * Input.sample_time .* repmat(Input.price_trade_grey_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- EMS_uni_reference.power_local_demand_green * Input.sample_time .* repmat(Input.price_trade_green_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- EMS_uni_reference.power_local_demand_grey * Input.sample_time .* repmat(Input.price_trade_grey_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities));
	profit_global_reference = double((EMS_uni_reference.power_supply_green-EMS_uni_reference.power_local_supply_green) * Input.sample_time .* repmat(Input.price_sell_green(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		+ (EMS_uni_reference.power_supply_grey-EMS_uni_reference.power_local_supply_grey) * Input.sample_time .* repmat(Input.price_sell_grey(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- (EMS_uni_reference.power_demand_grey-EMS_uni_reference.power_local_demand_green-EMS_uni_reference.power_local_demand_grey) * Input.sample_time .* repmat(Input.price_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities));
	profit_total_reference = profit_local_reference + profit_global_reference;
	profit_local_decentral = double(EMS_uni_decentral.power_local_supply_green * Input.sample_time .* repmat(Input.price_trade_green_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		+ EMS_uni_decentral.power_local_supply_grey * Input.sample_time .* repmat(Input.price_trade_grey_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- EMS_uni_decentral.power_local_demand_green * Input.sample_time .* repmat(Input.price_trade_green_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- EMS_uni_decentral.power_local_demand_grey * Input.sample_time .* repmat(Input.price_trade_grey_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities));
	profit_global_decentral = double((EMS_uni_decentral.power_supply_green-EMS_uni_decentral.power_local_supply_green) * Input.sample_time .* repmat(Input.price_sell_green(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		+ (EMS_uni_decentral.power_supply_grey-EMS_uni_decentral.power_local_supply_grey) * Input.sample_time .* repmat(Input.price_sell_grey(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- (EMS_uni_decentral.power_demand_grey-EMS_uni_decentral.power_local_demand_green-EMS_uni_decentral.power_local_demand_grey) * Input.sample_time .* repmat(Input.price_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities));
	profit_total_decentral = profit_local_decentral + profit_global_decentral;
	profit_local_central = double(EMS_uni_central.power_local_supply_green * Input.sample_time .* repmat(Input.price_trade_green_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		+ EMS_uni_central.power_local_supply_grey * Input.sample_time .* repmat(Input.price_trade_grey_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- EMS_uni_central.power_local_demand_green * Input.sample_time .* repmat(Input.price_trade_green_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- EMS_uni_central.power_local_demand_grey * Input.sample_time .* repmat(Input.price_trade_grey_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities));
	profit_global_central = double((EMS_uni_central.power_supply_green-EMS_uni_central.power_local_supply_green) * Input.sample_time .* repmat(Input.price_sell_green(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		+ (EMS_uni_central.power_supply_grey-EMS_uni_central.power_local_supply_grey) * Input.sample_time .* repmat(Input.price_sell_grey(1:Input.simulation_time/Input.sample_time)',1,Input.entities) ...
		- (EMS_uni_central.power_demand_grey-EMS_uni_central.power_local_demand_green-EMS_uni_central.power_local_demand_grey) * Input.sample_time .* repmat(Input.price_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities));
	profit_total_central = profit_local_central + profit_global_central;

	figure('Units','normalized','Position',[.1 .1 .8 .8]); box on; hold on; grid on;
		ax1 = subplot(1,3,1); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_local_reference),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Local, reference)');
			ax=gca; ax_left=ax_border(1); ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax2 = subplot(1,3,2); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_local_decentral),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Local, decentral)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_local_decentral(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1)+1*ax_width+1*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax3 = subplot(1,3,3); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_local_central),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Local, central)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_local_central(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1)+2*ax_width+2*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		linkaxes([ax1,ax2,ax3],'xy');
	
	figure('Units','normalized','Position',[.1 .1 .8 .8]); box on; hold on; grid on;
		ax1 = subplot(1,3,1); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_total_reference),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Total, reference)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_total_reference(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1); ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax2 = subplot(1,3,2); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_total_decentral),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Total, decentral)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_total_decentral(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1)+1*ax_width+1*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax3 = subplot(1,3,3); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_total_central),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Total, central)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_total_central(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1)+2*ax_width+2*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		linkaxes([ax1,ax2,ax3],'xy');
	
	figure('Units','normalized','Position',[.1 .1 .8 .8]); box on; hold on; grid on;
		ax1 = subplot(1,3,1); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_total_reference-profit_total_reference),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Total, reference)');
			ax=gca; ax_left=ax_border(1); ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax2 = subplot(1,3,2); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_total_decentral-profit_total_reference),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Total, decentral)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_total_decentral(:,i)-profit_total_reference(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1)+1*ax_width+1*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		ax3 = subplot(1,3,3); box on; hold on; grid on;
			plot(plot_sampling,cumsum(profit_total_central-profit_total_reference),'LineWidth',line_width);
			xlim(xlimtime);
			xlabel('Time / days'); ylabel('Profit / EUR');
			title('Cumulative Profit (Total, central)');
			for i = 1:Input.entities
				text(plot_sampling(end),sum(profit_total_central(:,i)-profit_total_reference(:,i)),['Peer ',sprintf('%03.0f',uint32(i)),' ',char(hex2dec('2192'))],'HorizontalAlignment','right')
			end
			ax=gca; ax_left=ax_border(1)+2*ax_width+2*ax_dist; ax_width=(1-ax_border(1)-ax_border(2)-2*ax_dist)/3; ax.Position=[ax_left,ax_bottom,ax_width,ax_height];
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		linkaxes([ax1,ax2,ax3],'xy');
	
	figure('Units','normalized','Position',[.1 .1 .8 .8]); box on; hold on; grid on;
		title('Cumulative Profit');
		plot(plot_sampling,cumsum(sum(profit_total_reference,2)),'LineWidth',line_width,'DisplayName','Reference');
		plot(plot_sampling,cumsum(sum(profit_total_decentral,2)),'LineWidth',line_width,'DisplayName','Decentral');
		plot(plot_sampling,cumsum(sum(profit_total_central,2)),'LineWidth',line_width,'DisplayName','Central');
		xlim(xlimtime);
		xlabel('Time / days'); ylabel('Profit / EUR');
		legend;
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		set(gca,'LooseInset',get(gca,'TightInset')); a = get(gca,'Position'); set(gca,'Position',[a(1) a(2) a(3)-0.002 a(4)-0.005]);
		clear a;

	welfare_decentral_total = sum(sum(profit_total_decentral - profit_total_reference,2));
	welfare_decentral_market = double(sum(sum(...
		+ EMS_uni_decentral.power_local_supply_green * Input.sample_time .* (repmat(Input.price_trade_green_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) - repmat(Input.price_trade_green_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities)) ...
		+ EMS_uni_decentral.power_local_supply_grey * Input.sample_time .* (repmat(Input.price_trade_grey_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) - repmat(Input.price_trade_grey_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities)) )));
	welfare_decentral_peers = welfare_decentral_total - welfare_decentral_market;
	welfare_central_total = sum(sum(profit_total_central - profit_total_reference,2));
	welfare_central_market = double(sum(sum(...
		+ EMS_uni_central.power_local_supply_green * Input.sample_time .* (repmat(Input.price_trade_green_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) - repmat(Input.price_trade_green_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities)) ...
		+ EMS_uni_central.power_local_supply_grey * Input.sample_time .* (repmat(Input.price_trade_grey_purchase(1:Input.simulation_time/Input.sample_time)',1,Input.entities) - repmat(Input.price_trade_grey_sell(1:Input.simulation_time/Input.sample_time)',1,Input.entities)) )));
	welfare_central_peers = welfare_central_total - welfare_central_market;
	welfare = [...
		welfare_decentral_peers,...
		welfare_decentral_market;...
		welfare_central_peers,...
		welfare_central_market;...
		];
	welfare_rel = [...
		welfare_decentral_peers/welfare_decentral_total,...
		welfare_decentral_market/welfare_decentral_total;...
		welfare_central_peers/welfare_central_total,...
		welfare_central_market/welfare_central_total;...
		];
	xlocation = [1,2];
	ylocation = [...
		welfare_decentral_peers/2,...
		welfare_decentral_peers+welfare_decentral_market/2;...
		welfare_central_peers/2,...
		welfare_central_peers+welfare_central_market/2;...
		];

	figure('Units','normalized','Position',[.1 .1 .8 .8]); box on; hold on; grid on;
		title('Economic Welfare');
		bar(xlocation,welfare,'stacked');
		xlim([.5,2.5]); xticks(xlocation); xticklabels({'Decentral','Central'});
		ylabel('Welfare / EUR');
		for i=1:size(welfare,1)
			for j=1:size(welfare,2)
				text(xlocation(1,i), ylocation(i,j), [sprintf('%.1f',welfare_rel(i,j)*100),'%'],'HorizontalAlignment','center','VerticalAlignment','middle');
			end
		end; clear i j xlocation ylocation;
		legend({'Welfare @Peers','Welfare @Market'},'Location','South');
		set(findall(gcf,'-property','FontName'),'FontName',font_name);
		set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
		set(gca,'LooseInset',get(gca,'TightInset')); a = get(gca,'Position'); set(gca,'Position',[a(1) a(2) a(3)-0.002 a(4)-0.005]);
		clear a;

%% save plots
figHandles = get(groot, 'Children');
	for i = 1:length(figHandles)
		if Input.plot_png; exportgraphics(figHandles(uint32(length(figHandles)+1-i)),[Input.path_results,Input.scenario_name,'_fig_',sprintf('%03.0f',uint32(i)),'.png'],'Resolution',150); end
		if Input.plot_pdf; exportgraphics(figHandles(uint32(length(figHandles)+1-i)),[Input.path_results,Input.scenario_name,'_fig_',sprintf('%03.0f',uint32(i)),'.pdf'],'BackgroundColor','none','ContentType','vector'); end
	end; clear i;
end

% clear unused variables
	clear font* line* plot* time* xlim* ax* E_* profit* welfare* figHandles;