# flex_opt


The tool was created by Stefan Englberger at the Institute for Electrical Energy Storage Technology at the Technical University of Munich. For efficient optimization, the tool was designed in Mathworks Matlab and also supports the Gurobi solver.


### **How to cite:**

[Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency between peer-to-peer networks and energy storages: A techno-economic proof for prosumers. Advances in Applied Energy, 3.](https://doi.org/10.1016/j.adapen.2021.100059)
