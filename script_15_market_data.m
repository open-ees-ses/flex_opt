%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

% grid charge
	Input.price_grid_charges = 0.0739 * ones(1,Input.profile_length); % [EUR/kWh]

% variable grid charge
	dummy = round(movmean(round(normrnd(0,.4,1,Input.profile_length)),1/Input.sample_time,'omitnan'));
	dummy(dummy<-1) = -1;
	dummy(dummy>+1) = +1;
	dummy(1) = 0;
	price_grid_charges_variable = dummy * 0.01;

% distribution price
	Input.price_distribution = 0.0706; % [EUR/kWh]

% surcharges
% https://www.bdew.de/service/daten-und-grafiken/strompreis-fuer-haushalte/
	Input.price_surcharges = 0.1573; % [EUR/kWh]

% price purchase
	Input.price_purchase = Input.price_grid_charges + Input.price_distribution + Input.price_surcharges + price_grid_charges_variable; % [EUR/kWh]

% price sell pv
% https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Unternehmen_Institutionen/ErneuerbareEnergien/ZahlenDatenInformationen/EEG_Registerdaten/EEG_Registerdaten_node.html#:~:text=1%20EEG%20).,Ausschreibungsergebnisse%20des%20Jahres%202018%20verwendet.&text=Damit%20liegt%20der%20dem%20Verg%C3%BCtungssatz,6%2C04%20Cent%20pro%20Kilowattstunde.
	Input.price_sell_green = 0.0845 * ones(size(Input.price_purchase)) - price_grid_charges_variable; % [EUR/kWh]

% price sell grey
	Input.price_sell_grey = 0.0280 * ones(size(Input.price_purchase)) - price_grid_charges_variable; % [EUR/kWh]

% trading price signals
	Input.price_trade_green = min(Input.price_purchase,Input.price_sell_green) ...
								+ Input.trading_factor * range([Input.price_purchase;Input.price_sell_green]);

	Input.price_trade_green_purchase = Input.price_trade_green + Input.trading_surcharge/2 * range([Input.price_purchase;Input.price_sell_green]);
	
	Input.price_trade_green_sell = Input.price_trade_green - Input.trading_surcharge/2 * range([Input.price_purchase;Input.price_sell_green]);
	
	Input.price_trade_grey = min(Input.price_purchase,Input.price_sell_grey) ...
								+ Input.trading_factor * range([Input.price_purchase;Input.price_sell_grey]);

	Input.price_trade_grey_purchase = Input.price_trade_grey + Input.trading_surcharge/2 * range([Input.price_purchase;Input.price_sell_grey]);
	
	Input.price_trade_grey_sell = Input.price_trade_grey - Input.trading_surcharge/2 * range([Input.price_purchase;Input.price_sell_grey]);