%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

function [EMS] = script_26_decentral_clearing(Input,EMS)

	arguments
        Input struct
        EMS struct
	end

% convert power to energy
	energy_supply_green = EMS.Variables.power_supply_green * Input.sample_time;
	energy_supply_grey = EMS.Variables.power_supply_grey * Input.sample_time;
	energy_demand_grey = EMS.Variables.power_demand_grey * Input.sample_time;
	
% minimum offered energy per sample time
	if Input.energy_offer_minimum > 0
		energy_supply_green(energy_supply_green<Input.energy_offer_minimum) = 0;
		energy_supply_grey(energy_supply_grey<Input.energy_offer_minimum) = 0;
		energy_demand_grey(energy_demand_grey<Input.energy_offer_minimum) = 0;
	end

% increment of offered energy per sample time
	if Input.energy_offer_increment > 0
		energy_supply_green = round(energy_supply_green/Input.energy_offer_increment)*Input.energy_offer_increment;
		energy_supply_grey = round(energy_supply_grey/Input.energy_offer_increment)*Input.energy_offer_increment;
		energy_demand_grey = round(energy_demand_grey/Input.energy_offer_increment)*Input.energy_offer_increment;
	end

% convert power to energy
	power_supply_green = energy_supply_green / Input.sample_time;
	power_supply_grey = energy_supply_grey / Input.sample_time;
	power_demand_grey = energy_demand_grey / Input.sample_time;
	
%% get necessary variables
	power_supply_green_total = sum(power_supply_green,2);
	power_supply_grey_total = sum(power_supply_grey,2);
	power_demand_grey_total = sum(power_demand_grey,2);

%% create optimization problem
% decision variables
	power_trade_green	= optimvar('power_trade_green',	Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',min(power_supply_green_total,power_demand_grey_total)); % [kWh] green power trade within local market
	power_trade_grey	= optimvar('power_trade_grey',	Input.optimization_time/Input.sample_time,1,'LowerBound',0,'UpperBound',min(power_supply_grey_total,power_demand_grey_total)); % [kWh] grey power trade within local market

% objective function with underlying profit functions
	opt_clearing = optimproblem('ObjectiveSense','maximize','Objective',...
		sum(power_trade_green * Input.sample_time .* (EMS.Variables.price_purchase-EMS.Variables.price_sell_green)...
		+ power_trade_grey * Input.sample_time .* (EMS.Variables.price_purchase-EMS.Variables.price_sell_grey)) ); % maxmize traded energy

% call constraints
	opt_clearing.Constraints.local_demand = ...
		power_trade_green + power_trade_grey <= power_demand_grey_total;

% surpress local offers outside trading horizon
	opt_clearing.Constraints.surpress_offers_1 = ...
		power_trade_green(Input.trading_horizon/Input.sample_time+1:end,:) == 0;
	opt_clearing.Constraints.surpress_offers_2 = ...
		power_trade_grey(Input.trading_horizon/Input.sample_time+1:end,:) == 0;

%% solve optimization
% define solver options
	opt_options = optimoptions('linprog','Display','none','MaxTime',Input.max_optimization_time,'OptimalityTolerance',Input.max_relative_optimization_gap);

% call solver
	[opt_variables,opt_objective,opt_flag,opt_settings] = solve(opt_clearing,'Options',opt_options);

% check optimization success
	if opt_flag ~= 1; error('Clearing was not successful'); end

% create optimization container
	Clearing.Problem	= opt_clearing;
	Clearing.Options	= opt_options;
	Clearing.Settings	= opt_settings;
	Clearing.Variables	= opt_variables;
	Clearing.Objective	= opt_objective;
	Clearing.ExitFlag	= opt_flag;

%% Update variables
	power_local_demand_green = Clearing.Variables.power_trade_green ./ power_demand_grey_total .* EMS.Variables.power_demand_grey;
	power_local_demand_green(isnan(power_local_demand_green)) = 0;
	power_local_demand_grey = Clearing.Variables.power_trade_grey ./ power_demand_grey_total .* EMS.Variables.power_demand_grey;
	power_local_demand_grey(isnan(power_local_demand_grey)) = 0;
	power_local_supply_green = Clearing.Variables.power_trade_green ./ power_supply_green_total .* EMS.Variables.power_supply_green;
	power_local_supply_green(isnan(power_local_supply_green)) = 0;
	power_local_supply_grey = Clearing.Variables.power_trade_grey ./ power_supply_grey_total .* EMS.Variables.power_supply_grey;
	power_local_supply_grey(isnan(power_local_supply_grey)) = 0;
	
% convert power to energy
	energy_local_demand_green = power_local_demand_green * Input.sample_time;
	energy_local_demand_grey = power_local_demand_grey * Input.sample_time;
	energy_local_supply_green = power_local_supply_green * Input.sample_time;
	energy_local_supply_grey = power_local_supply_grey * Input.sample_time;
	
% minimum offered energy per sample time
	if Input.energy_transaction_minimum > 0
		energy_local_demand_green(energy_local_demand_green<Input.energy_transaction_minimum) = 0;
		energy_local_demand_grey(energy_local_demand_grey<Input.energy_transaction_minimum) = 0;
		energy_local_supply_green(energy_local_supply_green<Input.energy_transaction_minimum) = 0;
		energy_local_supply_grey(energy_local_supply_grey<Input.energy_transaction_minimum) = 0;
	end

% increment of offered energy per sample time
	if Input.energy_transaction_increment > 0
		energy_local_demand_green = round(energy_local_demand_green/Input.energy_transaction_increment)*Input.energy_transaction_increment;
		energy_local_demand_grey = round(energy_local_demand_grey/Input.energy_transaction_increment)*Input.energy_transaction_increment;
		energy_local_supply_green = round(energy_local_supply_green/Input.energy_transaction_increment)*Input.energy_transaction_increment;
		energy_local_supply_grey = round(energy_local_supply_grey/Input.energy_transaction_increment)*Input.energy_transaction_increment;
	end

% convert power to energy
	EMS.Variables.power_local_demand_green = energy_local_demand_green / Input.sample_time;
	EMS.Variables.power_local_demand_grey = energy_local_demand_grey / Input.sample_time;
	EMS.Variables.power_local_supply_green = energy_local_supply_green / Input.sample_time;
	EMS.Variables.power_local_supply_grey = energy_local_supply_grey / Input.sample_time;

	EMS.Variables.power_global_demand_grey = EMS.Variables.power_demand_grey - EMS.Variables.power_local_demand_grey - EMS.Variables.power_local_demand_green;
	EMS.Variables.power_global_supply_green = EMS.Variables.power_supply_green - EMS.Variables.power_local_supply_green;
	EMS.Variables.power_global_supply_grey = EMS.Variables.power_supply_grey - EMS.Variables.power_local_supply_grey;

end