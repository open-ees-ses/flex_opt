%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

function [Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_lfp(state_of_charge,temperature,Q_loss_previous,Q_loss_calendar_previous,sample_time)

	% calculation based on: 
	% Naumann, M., Schimpe, M., Keil, P., Hesse, H., and Jossen, A. (2018). Analysis and modeling 
	% of calendar aging of a commercial LiFePO4/graphite cell. Journal of Energy Storage 17, 153–169.
	% Naumann, M., Spingler, F.B., and Jossen, A. (2020). Analysis and modeling of cycle aging of a 
	% commercial LiFePO4/graphite cell. Journal of Power Sources 451, 227666.

	depth_of_discharge = abs([0;diff(state_of_charge)]);
	Q_loss = zeros(size(state_of_charge));
	Q_loss_calendar = zeros(size(state_of_charge));
	Q_loss_cycle = zeros(size(state_of_charge));
	for i = 1:length(state_of_charge)
		% calendar degradation
		k_T = 0.000012571 * exp( -17126/8.3144598 * (1/temperature(i) - 1/298.15));
		k_SOC = 2.8575 * (state_of_charge(i) - 0.5)^3 + 0.60225;
		t_virtual = (Q_loss_previous / (k_T * k_SOC))^2;
		Q_loss_calendar(i) = ((k_T*k_SOC*(t_virtual+sample_time*3600)^0.5)-Q_loss_previous);
		% cycle degradation
		k_crate = 0.0630 * (depth_of_discharge(i)/sample_time) + 0.0971;
		k_doc = 4.0253 * (depth_of_discharge(i) - 0.6)^3 + 1.0923;
		fec_virtual = (Q_loss_previous*100 / (k_crate * k_doc))^2;
% 		CycAging_Cap = @(k_crate,k_doc,fec_virtual) k_crate.*k_doc./(2*fec_virtual.^0.5);
% 		CycAging_Cap = @(k_crate,k_doc,fec_virtual) k_crate.*k_doc.*(fec_virtual.^0.5);
		if fec_virtual == 0
			Q_loss_cycle(i) = 0;
		else
% 			Q_loss_cycle(i) = integral(@(fec_virtual)CycAging_Cap(k_crate,k_doc,fec_virtual), fec_virtual, fec_virtual+depth_of_discharge(i)/2)/100;
% 			Q_loss_cycle(i) = (CycAging_Cap(k_crate,k_doc,fec_virtual+depth_of_discharge(i)/2) - CycAging_Cap(k_crate,k_doc,fec_virtual))/100;
			Q_loss_cycle(i) = (k_crate.*k_doc.*((fec_virtual+depth_of_discharge(i)/2).^0.5) - k_crate.*k_doc.*(fec_virtual.^0.5))/100;
		end
		Q_loss(i) = Q_loss_calendar(i) + Q_loss_cycle(i);
		Q_loss_previous = Q_loss_previous + Q_loss(i);
		Q_loss_calendar_previous = Q_loss_calendar_previous + Q_loss_calendar(i);
	end

end
