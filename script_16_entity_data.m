%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

%% prediction discrepancy
	Input.prediction_discrepancy = zeros(1,Input.optimization_time/Input.sample_time);
	Input.prediction_discrepancy_load = zeros(Input.profile_length,Input.entities);
	Input.prediction_discrepancy_pv = zeros(Input.profile_length,Input.entities);
	Input.prediction_discrepancy_ev_drive = zeros(Input.profile_length,Input.entities);
	
	if Input.prediction_error == 1
		Input.prediction_discrepancy = linspace(0.08,1.5,Input.optimization_time/Input.sample_time);

		dummy = randi([-100,100],Input.entities,Input.profile_length);
		for i = 1:size(dummy,1)
			dummy(i,:) = movmean(dummy(i,:),1/Input.sample_time,'omitnan')/100;
		end
		Input.prediction_discrepancy_load = transpose(dummy);
		clear dummy i;

		dummy = randi([-100,100],Input.entities,Input.profile_length);
		for i = 1:size(dummy,1)
			dummy(i,:) = movmean(dummy(i,:),1/Input.sample_time,'omitnan')/100;
		end
		Input.prediction_discrepancy_pv = transpose(dummy);
		clear dummy i;

		dummy = randi([-100,100],Input.entities,Input.profile_length);
		for i = 1:size(dummy,1)
			dummy(i,:) = movmean(dummy(i,:),1/Input.sample_time,'omitnan')/100;
		end
		Input.prediction_discrepancy_ev_drive = transpose(dummy);
		clear dummy i;
	end

%% load data if available
	try load('ev_profiles.mat'); catch; end
	try load('profiles.mat'); catch; end

%% real load data
	try
	% use existing data if available
		for i = 1:Input.entities
			power_load(:,i)	= eval(['profile_power_active_load_',sprintf('%03d',Input.profile_load(i)),';']);
		end
		clear i;
		power_load		= repelem(double(power_load),1,1);
		power_load		= power_load ./ sum(power_load) .* Input.consumption_building * 60; % scaling of profile
		power_load		= repmat(power_load,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
		dummy			= zeros(Input.profile_length,Input.entities);
		for i = uint32(1:Input.profile_length); dummy(i,:) = mean(power_load(Input.simulation_start*60+(i-1)*(Input.sample_time*60)+1:Input.simulation_start*60+i*(Input.sample_time*60),:)); end; clear i;
		Input.power_load = dummy; clear dummy power_load;
	catch
	% create necessary data
		dummy = 0.3+0.7*rand(Input.profile_length,Input.entities) .* ones(1,Input.entities);
		dummy = movmean(dummy,6/Input.sample_time,'omitnan').^7;
		Input.power_load = dummy./sum(dummy) .* Input.consumption_building * (Input.profile_length*Input.sample_time)/(365*24)/Input.sample_time;
		clear dummy i power_load;
	end

%% real pv data
	try
	% use existing data if available
		for i = 1:Input.entities
			power_pv(:,i)	= eval(['profile_power_gen_',sprintf('%03d',Input.profile_pv(i)),';']);
		end
		clear i;
		power_pv		= repelem(double(power_pv),1,1);
		power_pv		= power_pv ./ max(power_pv) .* Input.peak_generation; % scaling of profile
		power_pv		= repmat(power_pv,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
		dummy			= zeros(Input.profile_length,Input.entities);
		for i = uint32(1:Input.profile_length); dummy(i,:) = mean(power_pv(Input.simulation_start*60+(i-1)*(Input.sample_time*60)+1:Input.simulation_start*60+i*(Input.sample_time*60),:)); end; clear i;
		Input.power_pv = dummy; clear dummy power_pv;
	catch
	% create necessary data
		dummy = ceil(Input.profile_length/24*Input.sample_time)+1;
		dummy = sin(linspace(0,2*pi*dummy,dummy*24/Input.sample_time)).*abs(sin(linspace(0,2*pi*dummy,dummy*24/Input.sample_time)));
		dummy(dummy<0) = 0; dummy = circshift(dummy,6/Input.sample_time); dummy = transpose(dummy);
		dummy = (0.7+0.3*rand(size(dummy,1),Input.entities)).*dummy; dummy(dummy<0) = 0;
		dummy = dummy./max(dummy) .* Input.peak_generation;
		Input.power_pv = dummy(mod(Input.simulation_start,24)/Input.sample_time+1:mod(Input.simulation_start,24)/Input.sample_time+Input.profile_length,:);
		clear dummy;
	end
	Input.power_pv_green = Input.power_pv .* Input.pv_green;
	Input.power_pv_grey = Input.power_pv .* (-Input.pv_green+1);

%% real ev data
	if exist('profile_ev_drive_distance','var') && exist('profile_ev_plugged_home','var')
	% use existing data if available
		ev_drive_distance = zeros(size(profile_ev_drive_distance,1),Input.entities);
		ev_plugged_home = zeros(size(profile_ev_drive_distance,1),Input.entities);
		for i = 1:Input.entities
			if Input.electric_vehicle(i) == 1
				ev_drive_distance(:,i) = profile_ev_drive_distance(:,Input.profile_ev(i));
				ev_plugged_home(:,i) = profile_ev_plugged_home(:,Input.profile_ev(i));
			end
		end
		power_ev_drive = ev_drive_distance ./ sum(ev_drive_distance) .* (Input.EV.avg_consumption / Input.sample_time .* Input.EV.avg_distance);
		power_ev_drive = repmat(power_ev_drive,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
		ev_plugged_home = repmat(ev_plugged_home,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
		power_ev_drive([1:Input.simulation_start/Input.sample_time,Input.simulation_start/Input.sample_time+Input.profile_length+1:end],:) = [];
		ev_plugged_home([1:Input.simulation_start/Input.sample_time,Input.simulation_start/Input.sample_time+Input.profile_length+1:end],:) = [];
		power_ev_drive(isnan(power_ev_drive)) = 0;
		Input.power_ev_drive = power_ev_drive;
		Input.ev_plugged_home = ev_plugged_home;
		clear i ev_drive_distance ev_plugged_home power_ev_drive;
	else
	% create necessary data
		Input.power_ev_drive = zeros(Input.profile_length,Input.entities);
		Input.ev_plugged_home = zeros(Input.profile_length,Input.entities);
	end

%% predicted data
	Input.power_load_prediction = zeros(Input.simulation_time/Input.rolling_horizon,Input.optimization_time/Input.sample_time,Input.entities);
	Input.power_pv_prediction = zeros(Input.simulation_time/Input.rolling_horizon,Input.optimization_time/Input.sample_time,Input.entities);
	Input.power_ev_drive_prediction = zeros(Input.simulation_time/Input.rolling_horizon,Input.optimization_time/Input.sample_time,Input.entities);
	Input.ev_plugged_home_prediction = zeros(Input.simulation_time/Input.rolling_horizon,Input.optimization_time/Input.sample_time,Input.entities);
	for i = uint32(1:size(Input.power_load_prediction,1))
		Input.power_load_prediction(i,:,:) = Input.power_load((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) + Input.power_load((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) .* Input.prediction_discrepancy_load(i:i-1+Input.optimization_time/Input.sample_time,:) .* Input.prediction_discrepancy';
		Input.power_pv_prediction(i,:,:) = Input.power_pv((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) + Input.power_pv((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) .* Input.prediction_discrepancy_load(i:i-1+Input.optimization_time/Input.sample_time,:) .* Input.prediction_discrepancy';
		Input.power_ev_drive_prediction(i,:,:) = Input.power_ev_drive((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) + Input.power_ev_drive((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:) .* Input.prediction_discrepancy_ev_drive(i:i-1+Input.optimization_time/Input.sample_time,:) .* Input.prediction_discrepancy';
		Input.ev_plugged_home_prediction(i,:,:) = Input.ev_plugged_home((i-1)*(Input.rolling_horizon/Input.sample_time)+1:(i-1)*(Input.rolling_horizon/Input.sample_time)+Input.optimization_time/Input.sample_time,:);
	end
	clear i;
	Input.power_load_prediction(Input.power_load_prediction<0) = 0;
	Input.power_pv_prediction(Input.power_pv_prediction<0) = 0;
	Input.power_ev_drive_prediction(Input.power_ev_drive_prediction<0) = 0;
	Input.power_pv_green_prediction = Input.power_pv_prediction;
	Input.power_pv_grey_prediction = Input.power_pv_prediction;
	for i = 1:size(Input.power_pv_prediction,1)
		for j = 1:size(Input.power_pv_prediction,2)
			for k = 1:size(Input.power_pv_prediction,3)
				if Input.pv_green(k) == 1
					Input.power_pv_grey_prediction(i,j,k) = 0;
				else
					Input.power_pv_green_prediction(i,j,k) = 0;
				end
				if Input.power_pv_prediction(i,j,k) > Input.peak_generation(k)
					Input.power_pv_prediction(i,j,k) = Input.peak_generation(k);
					Input.power_pv_green_prediction(i,j,k) = Input.peak_generation(k);
					Input.power_pv_grey_prediction(i,j,k) = Input.peak_generation(k);
				end
			end
		end
	end
	clear i j k;
	
clear comment profile*;

%% temperature data
	load('ambient_temperature_muc_2018.mat');
	Input.SESS.temperature_cell			= 25 * ones(Input.profile_length,1) + 273.15; % [K] cell temperature
	Input.EV.temperature_cell			= repelem(ambient_temperature_muc_2018,1/Input.sample_time,1) + 5 + 273.15; % [K] cell temperature
	Input.EV.temperature_cell			= [Input.EV.temperature_cell;Input.EV.temperature_cell(1:Input.profile_length-length(Input.EV.temperature_cell),1)];
% 	Input.EV.temperature_cell			= 15 * ones(Input.profile_length,1) + 273.15; % [K] cell temperature
clear ambient_temperature_muc_2018;