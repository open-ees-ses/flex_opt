%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

% optimization time has to be a multiple of the sample time
	if mod(Input.optimization_time,Input.sample_time) ~= 0
		error('Optimization time must be a multiple of the sample time.');
	end

% trading horizon has to be a multiple of the sample time
	if mod(Input.trading_horizon,Input.sample_time) ~= 0
		error('Trading horizon must be a multiple of the sample time.');
	end

% trading horizon has to be smaller than the optimization time
	if Input.trading_horizon > Input.optimization_time
		error('Trading horizon has to be smaller than the optimization time.');
	end

% rolling horizon has to be a multiple of the sample time
	if mod(Input.rolling_horizon,Input.sample_time) ~= 0
		error('Rolling horizon must be a multiple of the sample time.');
	end

% simulation time has to be a multiple of the rolling horizon
	if mod(Input.simulation_time,Input.rolling_horizon) ~= 0
		Input.simulation_time = Input.rolling_horizon * ceil(Input.simulation_time/Input.rolling_horizon);
		warning(['Simulation time must be a multiple of the rolling horizon period.',newline,' Simulation time corrected to ',num2str(Input.simulation_time/24),' days.']);
	end

	Input.profile_length				= (Input.simulation_time + Input.optimization_time - Input.rolling_horizon) / Input.sample_time;

	load('setting.mat');

%% stationary energy storage system (sess)
	Input.SESS.state_of_health_initial	= 1.0 * Input.stationary_storage; % [1] initial state of health of storage system
	Input.SESS.cell_chemistry			= repmat({'LFP'},1,Input.entities); % [NMC/LFP] battery cell chemistry
	Input.SESS.energy_nominal			= 7 * Input.stationary_storage; % [kWh] nominal energy content of storage system % Speichermonitoring BW
	Input.SESS.power_active				= 3.5 * Input.stationary_storage; % [kW] rated active power per inverter
	Input.SESS.state_of_charge_min		= 0.05 * Input.stationary_storage; % [1] minimum state of charge (lower bound)
	Input.SESS.state_of_charge_max		= 0.95 * Input.stationary_storage; % [1] maximum state of charge (upper bound)
	Input.SESS.state_of_charge_initial	= 0.50 * Input.stationary_storage; % [1] state of charge at the begin of simulation
	Input.SESS.state_of_charge_end		= 0.50 * Input.stationary_storage; % [1] state of charge at the end of simulation
	Input.SESS.efficiency_battery		= 0.99 * Input.stationary_storage; % [1] efficiency of battery cells
	Input.SESS.efficiency_inverter		= 0.95 * Input.stationary_storage; % [1] efficiency of power electronics
	Input.SESS.efficiency_charge		= prod([Input.SESS.efficiency_battery;Input.SESS.efficiency_inverter]); % [1] efficiency during charging
	Input.SESS.efficiency_discharge		= prod([Input.SESS.efficiency_battery;Input.SESS.efficiency_inverter]); % [1] efficiency during discharging
	Input.SESS.power_active_charge		= min(1.00*Input.SESS.energy_nominal/1,Input.SESS.power_active); % [kW] maximum active power during charging of ESS
	Input.SESS.power_active_discharge	= min(1.50*Input.SESS.energy_nominal/1,Input.SESS.power_active); % [kW] maximum active power during discharging of ESS
	Input.SESS.energy_self_discharge	= 0.006/(30.5*24/Input.sample_time) * Input.SESS.energy_nominal; % self-discharge depending on nominal energy content
	Input.SESS.invest_battery			= 0 * Input.SESS.energy_nominal; % invest costs for stationary storage system
	Input.SESS.efc_capability			= 6000 * Input.stationary_storage; % equivalent full cycles capability until system's end of life
	Input.SESS.capacity_cell_nominal	= 2.05 * Input.stationary_storage; % [Ah] nominal cell capacity

%% electric vehicle energy storage system (ev)
	Input.EV.state_of_health_initial	= 1.0 * Input.electric_vehicle; % [1] initial state of health of storage system
	Input.EV.cell_chemistry				= repmat({'NMC'},1,Input.entities); % [NMC/LFP] battery cell chemistry
	Input.EV.avg_consumption			= 0.189 .* Input.electric_vehicle; % [kWh/km] average energy consumption per km
% 	Input.EV.avg_distance				= round(normrnd(136,15,1,Input.entities))*100 .* Input.electric_vehicle; % [km] annual driving distance
	Input.EV.avg_distance				= avg_annual_distance(1:Input.entities) .* Input.electric_vehicle; % [km] annual driving distance
	Input.EV.energy_nominal				= 65 .* Input.electric_vehicle; % [kWh] nominal energy content of storage system
	Input.EV.state_of_charge_min		= 0.04 * Input.electric_vehicle; % [1] minimum state of charge (lower bound)
	Input.EV.state_of_charge_max		= 0.96 * Input.electric_vehicle; % [1] maximum state of charge (upper bound)
	Input.EV.power_active				= 11 * Input.electric_vehicle; % [kW] rated active power per inverter
	Input.EV.state_of_charge_initial	= 0.70 * Input.electric_vehicle; % [1] state of charge at the begin of simulation
	Input.EV.state_of_charge_end		= 0.70 * Input.electric_vehicle; % [1] state of charge at the end of simulation
	Input.EV.soc_preference				= 0.35 * Input.electric_vehicle; % [1] minimum soc threshold that should be allowed (max charge rate)
	Input.EV.efficiency_ev				= 0.894 * Input.electric_vehicle; % [1] efficiency of battery cells
	Input.EV.efficiency_charger			= 1.00 * Input.electric_vehicle; % [1] efficiency of power electronics
	Input.EV.efficiency_charge			= prod([Input.EV.efficiency_ev;Input.EV.efficiency_charger]); % [1] efficiency during charging
	Input.EV.efficiency_discharge		= prod([Input.EV.efficiency_ev;Input.EV.efficiency_charger]); % [1] efficiency during discharging
	Input.EV.power_active_charge		= min(1.00*Input.EV.energy_nominal/1,Input.EV.power_active); % [kW] maximum active power during charging of ESS
	Input.EV.power_active_discharge		= min(1.50*Input.EV.energy_nominal/1,Input.EV.power_active) .* Input.ev_bidirectional; % [kW] maximum active power during charging of ESS
	Input.EV.energy_self_discharge		= 0.006/(30.5*24/Input.sample_time) * Input.EV.energy_nominal; % self-discharge depending on nominal energy content
	Input.EV.invest_battery				= 0 * Input.EV.energy_nominal; % invest costs for stationary storage system
	Input.EV.efc_capability				= 1500 * Input.electric_vehicle; % equivalent full cycles capability until system's end of life
	Input.EV.capacity_cell_nominal		= 2.05 * Input.electric_vehicle; % [Ah] nominal cell capacity

%% entity electricity demand and supply
% 	Input.consumption_building			= round(normrnd(35,5,1,Input.entities))*100; % [kWh] annual energy consumption of building
	Input.consumption_building			= annual_consumption_building(1:Input.entities); % [kWh] annual energy consumption of building
% 	Input.peak_generation				=
% 	round(normrnd(9,1,1,Input.entities),1) .* Input.pv_system; % [kW] peak power of generation profile % Speichermonitoring BW
	Input.peak_generation				= peak_generation(1:Input.entities) .* Input.pv_system; % [kW] peak power of generation profile
	Input.feedin_limit					= 0.7 .* Input.peak_generation; % [kW] maximum feed-in power to grid