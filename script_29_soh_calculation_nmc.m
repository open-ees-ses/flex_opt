%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Institute for Electrical Energy Storage Technology
% 	Technical University of Munich
%
%
% Stefan Englberger, Archie C.Chapman, Wayes Tushar, Tariq Almomani, Stephen Snow, 
% Rolf Witzmann, Andreas Jossen, Holger Hesse. (2021). Evaluating the interdependency 
% between peer-to-peer networks and energy storages: A techno-economic proof for 
% prosumers. Advances in Applied Energy, 3. 
% https://doi.org/10.1016/j.adapen.2021.100059

function [Q_loss_calendar,Q_loss_cycle] = script_29_soh_calculation_nmc(state_of_charge,temperature,time,time_previous,charge_throughput_previous,capacity_cell_nominal)

	% calculation based on: 
	% Schmalstieg, J.; K�bitz, S.; Ecker, M.; Sauer, D.U. A holistic aging model for Li(NiMnCo)O2 based 18650 lithium-ion batteries. 
	% Journal of Power Sources 2014, 257, 325�334, doi:10.1016/j.jpowsour.2014.02.012.

% calculation of cell voltage from the state of charge [V]
	voltage = -17.91 * state_of_charge.^7 + 71.29 * state_of_charge.^6 + -111.1 * state_of_charge.^5 + 81.7 * state_of_charge.^4 + -24.96 * state_of_charge.^3 + 0.003646 * state_of_charge.^2 + 1.752 * state_of_charge + 3.33;

% average voltage [V]
	voltage_average = movmean(voltage,2);

% depth of discharge from the state of charge [1]
	depth_of_discharge = abs([0;diff(state_of_charge)]);

% charge throughput per cell [Ah]
	charge_throughput = charge_throughput_previous + cumsum(capacity_cell_nominal * depth_of_discharge);

	Q_loss_calendar = zeros(size(state_of_charge));
	Q_loss_cycle = zeros(size(state_of_charge));
	for i = 1:length(state_of_charge)
	% alpha and beta values for capacity loss calculations
		alpha_cap = (7.543 * voltage(i) - 23.75) * 10^6 * exp(-6976 / temperature(i));
		beta_cap = 7.348e-3 * (voltage_average(i) - 3.667)^2 + 7.600e-4 + 4.081e-3 * depth_of_discharge(i);

	% capacity loss calculations caused by calendar and cycle degradation
		if i == 1 && time_previous == 0
			Q_loss_calendar(i) = alpha_cap * time(i)^0.75;
			Q_loss_cycle(i) = beta_cap * charge_throughput(i)^0.5;
		elseif i == 1 && time_previous > 0
			Q_loss_calendar(i) = alpha_cap * (time(i)^0.75 - time_previous^0.75);
			Q_loss_cycle(i) = beta_cap * (charge_throughput(i)^0.5 - charge_throughput_previous^0.5);
		else
			Q_loss_calendar(i) = alpha_cap * (time(i)^0.75 - time(i-1)^0.75);
			Q_loss_cycle(i) = beta_cap * (charge_throughput(i)^0.5 - charge_throughput(i-1)^0.5);
		end
	end

end
